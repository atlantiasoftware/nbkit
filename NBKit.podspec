Pod::Spec.new do |s|

  s.name         = "NBKit"
  s.version      = "0.0.1"
  s.summary      = "Helpful NBKit utilities"
  s.description  = "Nick Bonatsakis Utils, the best lib ever."
  s.homepage     = "http://atlantiasoftware.com"
  s.license      = { :type => "MIT", :file => "LICENSE" }
  s.author             = { "Nick Bonatsakis" => "nbonatsakis@gmail.com" }
  s.platform     = :ios, "7.0"
  s.source       = { :git => "git@bitbucket.org:atlantiasoftware/nbkit.git", :tag => "0.0.1" }
  s.resources = "NBKit/**/*.{xib}", "NBKit/**/*.{png}"
  s.requires_arc = true
  
  s.subspec 'Services' do |services|
    services.source_files  = "NBKit", "NBKit/**/*.{h,m}"
    services.exclude_files   = 'NBKit/UI/NBAppearance.{h,m}'

    services.dependency 'AFNetworking'
    services.dependency 'RMStore'
    services.dependency 'SVProgressHUD'
    services.dependency 'CTFeedback'
    services.dependency 'UICKeyChainStore'
    services.dependency 'RZTransitions'
    services.dependency 'RZUtils'
    services.dependency 'iRate'

  end

  s.subspec 'Appearance' do |appearance|
    appearance.source_files   = 'NBKit/UI/NBAppearance.{h,m}'
  end
  
end
