//
//  UIView+IFEmptyState.h
//  InstaFeed
//
//  Created by Nicholas Bonatsakis on 2/17/15.
//  Copyright (c) 2015 Atlantia Software LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^NBInfoViewActionBlock)(void);

@interface UIView (NBInfoView)

- (void)nb_showInfoWithTitle:(NSString *)title
                       image:(UIImage *)image
                    animated:(BOOL)animated
                       onTap:(NBInfoViewActionBlock)tapBlock;
- (void)nb_hideInfoViewAnimated:(BOOL)animated;

- (BOOL)nb_isShowingInfoView;

@end
