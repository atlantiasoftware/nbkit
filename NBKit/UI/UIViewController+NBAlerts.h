//
//  UIViewController+NBAlerts.h
//  NBKit
//
//  Created by Nicholas Bonatsakis on 3/7/15.
//  Copyright (c) 2015 Atlantia Software LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (NBAlerts)

- (void)nb_showAlertWithTitle:(NSString *)title body:(NSString *)body;
- (void)nb_showAlertIncludingAppSettingsButtonWithTitle:(NSString *)title body:(NSString *)body;
- (void)nb_showAlertForError:(NSError *)error;

@end
