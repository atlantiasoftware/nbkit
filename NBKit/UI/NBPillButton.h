//
//  NPPillButton.h
//  NoPhone
//
//  Created by Nicholas Bonatsakis on 12/23/14.
//  Copyright (c) 2014 Atlantia Software LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NBPillButton : UIButton

@property (strong, nonatomic) UIColor *pillBorderColor;
@property (strong, nonatomic) UIColor *highlightedBorderColor;
@property (strong, nonatomic) UIColor *pillFillColor;
@property (strong, nonatomic) UIColor *highlightedFillColor;

@end
