//
//  NBCircularButton.h
//  InstaConcert
//
//  Created by Nicholas Bonatsakis on 11/8/13.
//  Copyright (c) 2013 Atlantia Software. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NBCircularButton : UIButton

@property (nonatomic, strong) CAShapeLayer *circleLayer;

@end
