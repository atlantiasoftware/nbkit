//
//  UIViewController+NBAlerts.m
//  NBKit
//
//  Created by Nicholas Bonatsakis on 3/7/15.
//  Copyright (c) 2015 Atlantia Software LLC. All rights reserved.
//

#import "UIViewController+NBAlerts.h"
#import "AFNetworkReachabilityManager.h"

@implementation UIViewController (NBAlerts)

- (void)nb_showAlertWithTitle:(NSString *)title body:(NSString *)body
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:body preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *OKAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Dismiss", nil) style:UIAlertActionStyleCancel handler:nil];
    [alertController addAction:OKAction];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)nb_showAlertIncludingAppSettingsButtonWithTitle:(NSString *)title body:(NSString *)body
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:body preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *settingsAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Settings", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        NSURL *appSettings = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
        [[UIApplication sharedApplication] openURL:appSettings];
    }];
    [alertController addAction:settingsAction];
    
    UIAlertAction *OKAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Dismiss", nil) style:UIAlertActionStyleCancel handler:nil];
    [alertController addAction:OKAction];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)nb_showAlertForError:(NSError *)error
{
    NSString *title = NSLocalizedString(@"Problem", nil);;
    
#ifdef DEBUG
    NSString *body = error.localizedDescription ?: NSLocalizedString(@"An unknown error occured", nil);
#else
    NSString *body = NSLocalizedString(@"There was an unknown problem encountered.", nil);
#endif
    
    if ( [[self networkErrorCodes] containsObject:@(error.code)] && [error.domain isEqualToString:NSURLErrorDomain] ) {
        title = NSLocalizedString(@"Network Unavailable", nil);
        body = NSLocalizedString(@"The network is currently unreachable. Please reconnect and try again.", nil);;
    }
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:body preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *OKAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Dismiss", nil) style:UIAlertActionStyleCancel handler:nil];
    [alertController addAction:OKAction];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

- (NSArray *)networkErrorCodes
{
    static NSArray *codesArray;
    if (![codesArray count]){
        @synchronized(self){
            const int codes[] = {
                kCFURLErrorCannotConnectToHost,     //-1004
                kCFURLErrorNetworkConnectionLost,   //-1005
                kCFURLErrorDNSLookupFailed,         //-1006
                kCFURLErrorResourceUnavailable,     //-1008
                kCFURLErrorNotConnectedToInternet,  //-1009
                kCFURLErrorInternationalRoamingOff, //-1018
                kCFURLErrorFileDoesNotExist,            //-1100
                kCFURLErrorNoPermissionsToReadFile,     //-1102
            };
            int size = sizeof(codes)/sizeof(int);
            NSMutableArray *array = [[NSMutableArray alloc] init];
            for (int i=0;i<size;++i){
                [array addObject:[NSNumber numberWithInt:codes[i]]];
            }
            codesArray = [array copy];
        }
    }
    return codesArray;
}

@end
