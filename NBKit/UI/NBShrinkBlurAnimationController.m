//
//  NBShrinkBlurAnimationController.m
//  NoPhone
//
//  Created by Nicholas Bonatsakis on 12/15/14.
//  Copyright (c) 2014 Atlantia Software LLC. All rights reserved.
//

#import "NBShrinkBlurAnimationController.h"
#import <objc/runtime.h>

static NSString *const kNBSnapshotImageKey = @"NBSnapshotImage";
static NSString *const kNBBlurEffectKey    = @"NBBlurEffect";

@implementation NBShrinkBlurAnimationController

- (void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext
{
    UIViewController *fromViewController = [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
    UIViewController *toViewController = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    UIView *container = [transitionContext containerView];
    
    toViewController.view.userInteractionEnabled = YES;
    
    if (!self.isPositiveAnimation)
    {
        
        UIVisualEffectView *effectView = (UIVisualEffectView *)objc_getAssociatedObject(fromViewController, &kNBBlurEffectKey);
        UIView *toSnapshot = (UIView *)objc_getAssociatedObject(fromViewController, &kNBSnapshotImageKey);
        
        [UIView animateWithDuration:[self transitionDuration:transitionContext] delay:0.0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            toSnapshot.layer.transform = CATransform3DIdentity;
            effectView.alpha = 0.0f;
            fromViewController.view.alpha = 0.0f;
            fromViewController.view.layer.transform = CATransform3DMakeScale(1.25f, 1.25f, 1.0f);
        } completion:^(BOOL finished) {
            toViewController.view.alpha = 1.0f;
            [toSnapshot removeFromSuperview];
            [effectView removeFromSuperview];
            [fromViewController.view removeFromSuperview];
            [transitionContext completeTransition:!transitionContext.transitionWasCancelled];
        }];
    }
    else
    {
        UIView *fromSnapshot = [fromViewController.view snapshotViewAfterScreenUpdates:YES];
        [container addSubview:fromViewController.view];
        [container addSubview:fromSnapshot];
        fromViewController.view.alpha = 0.0f;
        objc_setAssociatedObject(toViewController, &kNBSnapshotImageKey, fromSnapshot, OBJC_ASSOCIATION_RETAIN_NONATOMIC);

        UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:self.blurEffectStyle];
        UIVisualEffectView *effectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
        effectView.frame = fromViewController.view.bounds;
        effectView.alpha = 0.0f;
        [container addSubview:effectView];
        objc_setAssociatedObject(toViewController, &kNBBlurEffectKey, effectView, OBJC_ASSOCIATION_RETAIN_NONATOMIC);

        toViewController.view.frame = container.bounds;
        toViewController.view.alpha = 0.0f;
        toViewController.view.backgroundColor = [UIColor clearColor];
        toViewController.view.alpha = 0.0f;
        toViewController.view.layer.transform = CATransform3DMakeScale(1.2f, 1.2f, 1.0f);

        [container addSubview:toViewController.view];
        
        [UIView animateWithDuration:[self transitionDuration:transitionContext] delay:0.0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            fromSnapshot.layer.transform = CATransform3DMakeScale(0.85, 0.85, 1.0);
            effectView.alpha = 1.0f;
            toViewController.view.alpha = 1.0f;
            toViewController.view.layer.transform = CATransform3DIdentity;
        } completion:^(BOOL finished) {
            [transitionContext completeTransition:!transitionContext.transitionWasCancelled];
        }];
    }
}

- (NSTimeInterval)transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext
{
    return 0.4;
}

@end
