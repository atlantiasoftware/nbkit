//
//  NBAppearance.m
//  Jupiter
//
//  Created by Nicholas Bonatsakis on 5/17/14.
//
//

#import "NBAppearance.h"

@implementation UIView (NBAppearance)

- (UIImage *)nb_toImage
{
    UIGraphicsBeginImageContext(self.bounds.size);
    [self.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *resultingImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return resultingImage;
}

@end

@implementation UIColor (NBAppearance)

- (UIColor*)nb_colorWithAlpha:(CGFloat)alpha
{
    CGFloat r, g, b;
    [self getRed:&r green:&g blue:&b alpha:nil];
    return [UIColor colorWithRed:r green:g blue:b alpha:alpha];
}

- (UIColor *)nb_colorAdjustedByPercent:(CGFloat)adjustPercent
{
    CGFloat h, s, b, a;
    if ([self getHue:&h saturation:&s brightness:&b alpha:&a])
        return [UIColor colorWithHue:h
                          saturation:s
                          brightness:b * adjustPercent
                               alpha:a];
    return nil;
}

@end

@interface NBAppearance ()

@property (strong, nonatomic) NSCache *imageCache;

@end

@implementation NBAppearance

+ (instancetype)shared;
{
    static NBAppearance *s = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        s = [[self alloc] init];
    });
    
    return s;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.imageCache = [[NSCache alloc] init];
    }
    return self;
}

#pragma mark - Shapes

- (UIImage *)circleWithRadius:(CGFloat)radius borderWidth:(CGFloat)borderWidth borderColor:(UIColor *)borderColor bgColor:(UIColor *)bgColor
{
    NSString *key = [NSString stringWithFormat:@"%@-%f-%f-%@-%@",
                     NSStringFromSelector(_cmd),
                     radius, borderWidth,
                     [self stringForColor:borderColor],
                     [self stringForColor:bgColor]];
    
    UIImage *finalImage = [self.imageCache objectForKey:key];
    
    if (finalImage == nil) {
        CGRect layerRect = CGRectMake(0.0f, 0.0f, radius * 2, radius * 2);
        CAShapeLayer *layer = [CAShapeLayer layer];
        layer.bounds = layerRect;
        layer.path = [[UIBezierPath bezierPathWithOvalInRect:layerRect] CGPath];
        layer.lineWidth= borderWidth;
        layer.strokeColor = borderColor.CGColor;
        layer.fillColor = bgColor.CGColor;
        layer.backgroundColor = [UIColor clearColor].CGColor;
        layer.contentsScale = [UIScreen mainScreen].scale;
        
        [layer setNeedsDisplay];
        UIGraphicsBeginImageContext(layerRect.size);
        [layer renderInContext:UIGraphicsGetCurrentContext()];
        finalImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        [self.imageCache setObject:finalImage forKey:key];
    }
    
    return finalImage;
}

- (UIImage *)rectWithSize:(CGSize)rectSize borderWidth:(CGFloat)borderWidth borderColor:(UIColor *)borderColor bgColor:(UIColor *)bgColor
{
    NSString *key = [NSString stringWithFormat:@"%@-%f-%f-%f-%@-%@",
                     NSStringFromSelector(_cmd),
                     rectSize.width, rectSize.height, borderWidth,
                     [self stringForColor:borderColor],
                     [self stringForColor:bgColor]];
    
    UIImage *finalImage = [self.imageCache objectForKey:key];
    
    if (finalImage == nil) {
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, rectSize.width, rectSize.height)];
        CALayer *layer = view.layer;
        layer.borderWidth = borderWidth;
        layer.borderColor = borderColor.CGColor;
        view.backgroundColor = bgColor;
        layer.contentsScale = [UIScreen mainScreen].scale;
        
        [layer setNeedsDisplay];
        
        UIGraphicsBeginImageContext(layer.bounds.size);
        [layer renderInContext:UIGraphicsGetCurrentContext()];
        finalImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        [self.imageCache setObject:finalImage forKey:key];
    }
    
    return finalImage;
}

- (UIImage *)tintedImageNamed:(NSString *)imageName withColor:(UIColor *)color
{
    NSString *key = [NSString stringWithFormat:@"%@-%@", imageName, [self stringForColor:color]];
    UIImage *imageToReturn = [self.imageCache objectForKey:key];
    if ( imageToReturn == nil ) {
        UIImage *sourceImage = [UIImage imageNamed:imageName];
        if ( sourceImage != nil ) {
            UIGraphicsBeginImageContextWithOptions(sourceImage.size, NO, sourceImage.scale);
            CGContextRef context = UIGraphicsGetCurrentContext();
            CGContextTranslateCTM(context, 0, sourceImage.size.height);
            CGContextScaleCTM(context, 1.0, -1.0);
            CGContextSetBlendMode(context, kCGBlendModeNormal);
            CGRect rect = CGRectMake(0, 0, sourceImage.size.width, sourceImage.size.height);
            CGContextClipToMask(context, rect, sourceImage.CGImage);
            [color setFill];
            CGContextFillRect(context, rect);
            imageToReturn = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            
            [self.imageCache setObject:imageToReturn forKey:key];
        }
        
    }
    
    return imageToReturn;
}

#pragma mark - Rendering



#pragma mark - Util

- (NSString *)stringForColor:(UIColor *)color
{
    CGColorRef colorRef = color.CGColor;
    return [CIColor colorWithCGColor:colorRef].stringRepresentation;
}

@end
