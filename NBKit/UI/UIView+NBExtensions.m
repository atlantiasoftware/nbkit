//
//  UIView+NBExtensions.m
//  InstaConcert
//
//  Created by Nicholas Bonatsakis on 11/3/13.
//  Copyright (c) 2013 Atlantia Software. All rights reserved.
//

#import "UIView+NBExtensions.h"

@implementation UIView (NBExtensions)

- (void)nb_setBorderColor:(UIColor *)color
{
    self.layer.borderColor = color.CGColor;
}

- (void)nb_setBorderWidth:(CGFloat)width
{
    self.layer.borderWidth = width;
}

- (void)nb_setBorderRadius:(CGFloat)radius
{
    self.layer.cornerRadius = radius;
}

- (UIImage *)toImage
{
    UIGraphicsBeginImageContext(self.bounds.size);
    [self.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *resultingImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return resultingImage;
}

@end
