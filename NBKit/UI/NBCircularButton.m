//
//  NBCircularButton.m
//  InstaConcert
//
//  Created by Nicholas Bonatsakis on 11/8/13.
//  Copyright (c) 2013 Atlantia Software. All rights reserved.
//

#import "NBCircularButton.h"

@interface NBCircularButton ()

@end

@implementation NBCircularButton

- (void)willMoveToWindow:(UIWindow *)newWindow
{
    [super willMoveToWindow:newWindow];
    
    if (!self.circleLayer) {
        
        UIColor *color = self.tintColor;
        
        self.circleLayer = [CAShapeLayer layer];
        
        CGFloat min = MIN(self.bounds.size.width, self.bounds.size.height);
        [self.circleLayer setBounds:CGRectMake(0.0f, 0.0f, min, min)];
        [self.circleLayer setPosition:CGPointMake(self.bounds.size.width/2, self.bounds.size.height/2)];
        
        UIBezierPath *path = [UIBezierPath bezierPathWithOvalInRect:CGRectMake(0.0f, 0.0f, min, min)];
        [self.circleLayer setPath:[path CGPath]];
        
        [self.circleLayer setStrokeColor:[color CGColor]];
        
        [self.circleLayer setLineWidth:1.0f];
        [self.circleLayer setFillColor:[[UIColor clearColor] CGColor]];
        
        [self.layer insertSublayer:self.circleLayer atIndex:0];
    }
}

@end
