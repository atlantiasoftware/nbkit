//
//  NPPillButton.m
//  NoPhone
//
//  Created by Nicholas Bonatsakis on 12/23/14.
//  Copyright (c) 2014 Atlantia Software LLC. All rights reserved.
//

#import "NBPillButton.h"

@implementation NBPillButton

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (void)commonInit
{
    self.layer.masksToBounds = YES;
    self.layer.contentsScale = [UIScreen mainScreen].scale;
    self.imageView.tintColor = self.pillBorderColor;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    self.layer.cornerRadius = self.bounds.size.height / 2;
}

- (void)setPillBorderColor:(UIColor *)pillBorderColor
{
    _pillBorderColor = pillBorderColor;
    self.layer.borderColor = pillBorderColor.CGColor;
    self.imageView.tintColor = self.pillBorderColor;
}

- (void)setPillFillColor:(UIColor *)pillFillColor
{
    _pillFillColor = pillFillColor;
    self.backgroundColor = pillFillColor;
}

- (void)setHighlighted:(BOOL)highlighted
{
    [super setHighlighted:highlighted];
    
    // slightly delay un-highlight so that quick changes still appear
    if (!highlighted) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.01 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            
            if ( self.highlightedBorderColor) {
                self.layer.borderColor = self.pillBorderColor.CGColor;
            }
            
            if ( self.highlightedFillColor) {
                self.backgroundColor = self.pillFillColor;
            }
            
            self.imageView.tintColor = self.pillBorderColor;
            
            [self.layer setNeedsDisplay];
        });
    }
    else {
        if ( self.highlightedBorderColor) {
            self.layer.borderColor = self.highlightedBorderColor.CGColor;
        }
        
        if ( self.highlightedFillColor) {
            self.backgroundColor = self.highlightedFillColor;
        }
        
        self.imageView.tintColor = self.highlightedBorderColor;
        
        [self.layer setNeedsDisplay];
    }
}

@end
