//
//  NBShrinkBlurAnimationController.h
//  NoPhone
//
//  Created by Nicholas Bonatsakis on 12/15/14.
//  Copyright (c) 2014 Atlantia Software LLC. All rights reserved.
//

@import Foundation;
@import UIKit;
#import "RZAnimationControllerProtocol.h"

@interface NBShrinkBlurAnimationController : NSObject <RZAnimationControllerProtocol>

@property (nonatomic, assign) BOOL isPositiveAnimation;
@property (assign, nonatomic) UIBlurEffectStyle blurEffectStyle;

@end
