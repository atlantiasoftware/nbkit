//
//  NBInfoView.h
//  InstaFeed
//
//  Created by Nicholas Bonatsakis on 2/17/15.
//  Copyright (c) 2015 Atlantia Software LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NBInfoView : UIView <UIAppearanceContainer>

@property (strong, nonatomic) NSDictionary *titleTextAttributes UI_APPEARANCE_SELECTOR;
@property (strong, nonatomic) UIColor *infoBackgroundColor UI_APPEARANCE_SELECTOR;
@property (strong, nonatomic) UIColor *imageTintColor UI_APPEARANCE_SELECTOR;

- (instancetype)initWithTitle:(NSString *)title image:(UIImage *)image;

@end
