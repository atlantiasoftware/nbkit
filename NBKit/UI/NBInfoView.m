//
//  NBInfoView.m
//  InstaFeed
//
//  Created by Nicholas Bonatsakis on 2/17/15.
//  Copyright (c) 2015 Atlantia Software LLC. All rights reserved.
//

#import "NBInfoView.h"
#import "UIView+RZAutolayoutHelpers.h"

@interface NBInfoView ()

@property (weak, nonatomic) UILabel *titleLabel;
@property (weak, nonatomic) UIImageView *imageView;

@end

@implementation NBInfoView

- (instancetype)initWithTitle:(NSString *)title image:(UIImage *)image
{
    self = [super initWithFrame:CGRectZero];
    if (self) {
        [self configureViewsWithTitle:title image:image];
    }
    return self;
}

- (void)configureViewsWithTitle:(NSString *)title image:(UIImage *)image
{
    self.translatesAutoresizingMaskIntoConstraints = NO;
    
    UIView *containerView = [[UIView alloc] init];
    containerView.translatesAutoresizingMaskIntoConstraints = NO;
    containerView.backgroundColor = [UIColor clearColor];

    CGFloat vPadding = 30.0f;
    
    UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
    imageView.backgroundColor = [UIColor clearColor];
    imageView.translatesAutoresizingMaskIntoConstraints = NO;
    [containerView addSubview:imageView];
    [imageView rz_pinSizeTo:image.size];
    [imageView rz_centerHorizontallyInContainer];
    CGFloat imageVPadding = image != nil ? vPadding : 0.0f;
    [imageView rz_pinTopSpaceToSuperviewWithPadding:imageVPadding];
    _imageView = imageView;
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
    label.translatesAutoresizingMaskIntoConstraints = NO;
    label.text = title;
    label.numberOfLines = 0;
    label.textAlignment = NSTextAlignmentCenter;
    [containerView addSubview:label];
    [label rz_centerHorizontallyInContainer];
    [label rz_pinWidthTo:300.0f];
    [label rz_pinBottomSpaceToSuperviewWithPadding:vPadding];
    NSLayoutConstraint *vSpace = [NSLayoutConstraint constraintWithItem:imageView
                                                              attribute:NSLayoutAttributeBottom
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:label
                                                              attribute:NSLayoutAttributeTop
                                                             multiplier:1.0f
                                                               constant:-vPadding];
    [containerView addConstraint:vSpace];
    
    _titleLabel = label;
    
    [self addSubview:containerView];
    [containerView rz_centerVerticallyInContainer];
    [containerView rz_pinLeftSpaceToSuperviewWithPadding:20.0f];
    [containerView rz_pinRightSpaceToSuperviewWithPadding:20.0f];
}

#pragma mark - Properties

- (void)setInfoBackgroundColor:(UIColor *)backgroundColor
{
    self.backgroundColor = backgroundColor;
}

- (void)setTitleTextAttributes:(NSDictionary *)titleTextAttributes
{
    _titleTextAttributes = titleTextAttributes;
    
    if ( titleTextAttributes[NSFontAttributeName] ) {
        _titleLabel.font = titleTextAttributes[NSFontAttributeName];
    }
    
    if ( titleTextAttributes[NSForegroundColorAttributeName] ) {
        _titleLabel.textColor = titleTextAttributes[NSForegroundColorAttributeName];
    }
}

- (void)setImageTintColor:(UIColor *)imageTintColor
{
    _imageTintColor = imageTintColor    ;
    _imageView.tintColor = imageTintColor;
}

@end
