//
//  UIView+NBExtensions.h
//  InstaConcert
//
//  Created by Nicholas Bonatsakis on 11/3/13.
//  Copyright (c) 2013 Atlantia Software. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (NBExtensions)

- (void)nb_setBorderColor:(UIColor *)color;
- (void)nb_setBorderWidth:(CGFloat)width;
- (void)nb_setBorderRadius:(CGFloat)radius;

- (UIImage *)toImage;

@end
