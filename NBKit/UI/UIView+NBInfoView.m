//
//  UIView+IFEmptyState.m
//  InstaFeed
//
//  Created by Nicholas Bonatsakis on 2/17/15.
//  Copyright (c) 2015 Atlantia Software LLC. All rights reserved.
//

#import "UIView+NBInfoView.h"
#import "UIView+RZAutolayoutHelpers.h"
#import "NBInfoView.h"
#import <objc/runtime.h>

@implementation UIView (NBInfoView)

- (void)nb_showInfoWithTitle:(NSString *)title
                       image:(UIImage *)image
                    animated:(BOOL)animated
                       onTap:(NBInfoViewActionBlock)tapBlock
{
    NBInfoView *infoView = [[NBInfoView alloc] initWithTitle:title image:image];
    infoView.alpha = 0.0f;
    [self addSubview:infoView];
    [infoView rz_fillContainerWithInsets:UIEdgeInsetsZero];
    
    NSTimeInterval duration = animated ? 0.3f : 0.0f;
    [UIView animateWithDuration:duration animations:^{
        infoView.alpha = 1.0f;
    } completion:^(BOOL finished) {
        if ( tapBlock != nil ) {
            UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleActionTap:)];
            [infoView addGestureRecognizer:tap];
            [self setNBTapActionBlock:tapBlock];
        }
        
        if ( [self NBInfoView] != nil ) {
            [[self NBInfoView] removeFromSuperview];
        }
        
        [self setNBInfoView:infoView];
    }];

}

- (void)nb_hideInfoViewAnimated:(BOOL)animated
{
    if ( [self NBInfoView] != nil  ) {
        NSTimeInterval duration = animated ? 0.3f : 0.0f;
        [UIView animateWithDuration:duration animations:^{
            [self NBInfoView].alpha = 0.0f;
        } completion:^(BOOL finished) {
            [[self NBInfoView] removeFromSuperview];
            [self setNBInfoView:nil];
            [self setNBTapActionBlock:nil];
        }];
    }
}

- (void)handleActionTap:(id)sender
{
    if ( [self NBTapActionBlock] != nil ) {
        [self NBTapActionBlock]();
        [self setNBTapActionBlock:nil];
    }
}

- (BOOL)nb_isShowingInfoView
{
    return [self NBInfoView] != nil;
}

#pragma mark - Properties

- (NBInfoView *)NBInfoView
{
    return objc_getAssociatedObject(self, @selector(NBInfoView));
}

- (void)setNBInfoView:(NBInfoView *)infoView
{
    objc_setAssociatedObject(self, @selector(NBInfoView), infoView, OBJC_ASSOCIATION_RETAIN);
}

- (NBInfoViewActionBlock)NBTapActionBlock
{
    return objc_getAssociatedObject(self, @selector(NBTapActionBlock));
}

- (void)setNBTapActionBlock:(NBInfoViewActionBlock)block
{
    objc_setAssociatedObject(self, @selector(NBTapActionBlock), block, OBJC_ASSOCIATION_COPY);
}

@end
