//
//  NBActionSheet.h
//  OvulinePregnancy
//
//  Created by Nicholas Bonatsakis on 8/26/13.
//  Copyright (c) 2013 RaizLabs. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^NBActionSheetCompletion)(void);

@interface NBActionSheet : UIActionSheet

- (id)initWithTitle:(NSString *)title;
- (void)setCancelButtonTitle:(NSString *)title actionHandler:(NBActionSheetCompletion)completion;
- (void)setDestructiveButtonTitle:(NSString *)title actionHandler:(NBActionSheetCompletion)completion;
- (void)addButtonWithTitle:(NSString *)title actionHandler:(NBActionSheetCompletion)completion;

@end
