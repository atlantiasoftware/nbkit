//
//  NBAppearance.h
//  Jupiter
//
//  Created by Nicholas Bonatsakis on 5/17/14.
//
//

@import UIKit;
@import Foundation;

@interface UIView (NBAppearance)
- (UIImage *)nb_toImage;
@end


@interface UIColor (NBAppearance)
- (UIColor*)nb_colorWithAlpha:(CGFloat)alpha;
- (UIColor *)nb_colorAdjustedByPercent:(CGFloat)adjustPercent;
@end

@interface NBAppearance : NSObject

+ (instancetype)shared;

- (UIImage *)circleWithRadius:(CGFloat )radius borderWidth:(CGFloat)borderWidth borderColor:(UIColor *)borderColor bgColor:(UIColor *)bgColor;
- (UIImage *)rectWithSize:(CGSize)rectSize borderWidth:(CGFloat)borderWidth borderColor:(UIColor *)borderColor bgColor:(UIColor *)bgColor;
- (UIImage *)tintedImageNamed:(NSString *)imageName withColor:(UIColor *)color;

@end
