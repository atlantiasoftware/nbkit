//
//  NBSharingService.m
//  InstaConcert
//
//  Created by Nicholas Bonatsakis on 11/7/13.
//  Copyright (c) 2013 Atlantia Software. All rights reserved.
//

#import "NBSharingService.h"
#import "NBActionSheet.h"
@import StoreKit;
@import MessageUI;
#import <CTFeedback/CTFeedbackViewController.h>
#import "NBKitConfig.h"
#import "iRate.h"
#import "NBShrinkBlurAnimationController.h"
#import "NBTellFriendViewController.h"

NSString *const NBSharingServiceDidTellFriend = @"com.atlantia.SharingService.DidTellFriend";

@interface NBSharingService () <SKStoreProductViewControllerDelegate, MFMailComposeViewControllerDelegate, UIViewControllerTransitioningDelegate>

@property (strong, nonatomic) NBShrinkBlurAnimationController *blurTransition;

@end

@implementation NBSharingService

+ (NBSharingService *)shared;
{
    static NBSharingService *s = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        s = [[NBSharingService alloc] init];
    });
    return s;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.blurTransition = [[NBShrinkBlurAnimationController alloc] init];
        self.blurTransition.blurEffectStyle = UIBlurEffectStyleDark;
    }
    return self;
}

- (void)showLikeInViewController:(UIViewController *)viewController
{
    [self showLikeInViewController:viewController anchorViewForPad:nil anchorFrame:CGRectNull];
}

- (void)showLikeInViewController:(UIViewController *)viewController anchorViewForPad:(UIView *)anchorView anchorFrame:(CGRect)frame
{
    UIAlertControllerStyle style = [UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone ? UIAlertControllerStyleActionSheet : UIAlertControllerStyleAlert;
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Like App", nil) message:nil preferredStyle:style];
    
    UIAlertAction *rateAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Rate App", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [[iRate sharedInstance] openRatingsPageInAppStore];
    }];
    [alertController addAction:rateAction];
    
    UIAlertAction *shareAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Share with Friends", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self showTellFriend];
    }];
    [alertController addAction:shareAction];
    [alertController addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style:UIAlertActionStyleCancel handler:nil]];
    
    if (anchorView) {
        alertController.popoverPresentationController.sourceView = anchorView;
        alertController.popoverPresentationController.sourceRect = frame;
    }
    
    if ( alertController != nil ) {
        [viewController presentViewController:alertController animated:YES completion:nil];
    }
    else {
        [[iRate sharedInstance] openRatingsPageInAppStore];
    }
}

- (void)productViewControllerDidFinish:(SKStoreProductViewController *)viewController
{
    [viewController dismissViewControllerAnimated:YES completion:nil];
}

- (void)showProblemInViewController:(UIViewController *)viewController
{    
    CTFeedbackViewController *feedbackViewController = [CTFeedbackViewController controllerWithTopics:CTFeedbackViewController.defaultTopics localizedTopics:CTFeedbackViewController.defaultLocalizedTopics];
    feedbackViewController.toRecipients = @[ [NBKitConfig shared].feedbackEmailAddress ];
    UINavigationController *navVC = [[UINavigationController alloc] initWithRootViewController:feedbackViewController];
    [viewController presentViewController:navVC animated:YES completion:nil];
}

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    [controller dismissViewControllerAnimated:YES completion:nil];
}

- (void)showTellFriend
{
    UIViewController *viewController = [self topViewController:[UIApplication sharedApplication].keyWindow.rootViewController];
    NBTellFriendViewController *tellVC = [[NBTellFriendViewController alloc] init];
    tellVC.transitioningDelegate = self;
    [viewController presentViewController:tellVC animated:YES completion:nil];
}

#pragma mark - Util

- (NSString *)displayName
{
    return [[NSBundle mainBundle] infoDictionary] [@"CFBundleDisplayName"];
}

#pragma mark - Transition

- (id <UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source
{
    self.blurTransition.isPositiveAnimation = YES;
    return self.blurTransition;
}

- (id <UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed
{
    self.blurTransition.isPositiveAnimation = NO;
    return self.blurTransition;
}

#pragma mark - Util

- (UIViewController *)topViewController:(UIViewController *)viewController
{
    if (viewController.presentedViewController != nil) {
        return [self topViewController:viewController.presentedViewController];
    }
    else {
        return viewController;
    }
}


@end
