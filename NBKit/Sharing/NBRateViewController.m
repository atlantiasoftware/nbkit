//
//  NBRateViewController.m
//  NBKit
//
//  Created by Nicholas Bonatsakis on 8/9/14.
//  Copyright (c) 2014 Atlantia Software LLC. All rights reserved.
//

#import "NBRateViewController.h"
#import "iRate.h"
#import "NBKitConfig.h"
#import "NBRatingService.h"

#define kNBDefaultRateMessage NSLocalizedString(@"We'd like to sincerely thank you for trying out %@. If you could spare a quick moment to give us an honest rating on the App Store, it would make all the difference!", nil)

@interface NBRateViewController ()

@property (weak, nonatomic) IBOutlet UILabel *messageLabel;
@property (weak, nonatomic) IBOutlet UIButton *rateButton;
@property (weak, nonatomic) IBOutlet UIButton *noThanksButton;
@property (weak, nonatomic) CALayer *gradientLayer;

@end

@implementation NBRateViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSString *ratingMessage = [NBKitConfig shared].ratingMessage;
    self.messageLabel.text = ratingMessage ?: [NSString stringWithFormat:kNBDefaultRateMessage, [NBKitConfig shared].appDisplayName];
    
    [self styleButton];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)styleButton
{
    self.rateButton.layer.cornerRadius = self.rateButton.bounds.size.height / 2.0f;
    self.rateButton.backgroundColor = [NBKitConfig shared].appTintColor ?: [UIColor blueColor];

    self.noThanksButton.layer.cornerRadius = self.rateButton.bounds.size.height / 2.0f;
    self.noThanksButton.backgroundColor = [UIColor clearColor];
    self.noThanksButton.layer.borderColor = [UIColor whiteColor].CGColor;
    self.noThanksButton.layer.borderWidth = 1.0f;
}

#pragma mark - Actions

- (IBAction)rateTapped:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:NBRatingServiceDidRateNotification object:nil];
    
    [iRate sharedInstance].ratedThisVersion = YES;
    [self dismissViewControllerAnimated:YES completion:^{
        [[iRate sharedInstance] openRatingsPageInAppStore];
    }];
}

- (IBAction)noThanksTapped:(id)sender
{
    [iRate sharedInstance].declinedThisVersion  = YES;
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
