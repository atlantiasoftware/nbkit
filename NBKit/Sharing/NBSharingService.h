//
//  NBSharingService.h
//  InstaConcert
//
//  Created by Nicholas Bonatsakis on 11/7/13.
//  Copyright (c) 2013 Atlantia Software. All rights reserved.
//

#import <Foundation/Foundation.h>

OBJC_EXTERN NSString *const NBSharingServiceDidTellFriend;

@interface NBSharingService : NSObject

+ (NBSharingService *)shared;

- (void)showLikeInViewController:(UIViewController *)viewController;
- (void)showLikeInViewController:(UIViewController *)viewController anchorViewForPad:(UIView *)anchorView anchorFrame:(CGRect)frame;
- (void)showProblemInViewController:(UIViewController *)viewController;
- (void)showTellFriend;

@end
