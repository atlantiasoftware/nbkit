//
//  NBTellFriendViewController.m
//  NBKit
//
//  Created by Nicholas Bonatsakis on 4/26/15.
//  Copyright (c) 2015 Atlantia Software LLC. All rights reserved.
//

#import "NBTellFriendViewController.h"
#import "NBPillButton.h"
#import "NBKitConstants.h"
#import "UIColor+RZExtensions.h"
#import "NBAppearance.h"
#import "UIView+RZAutoLayoutHelpers.h"
#import "NBSharingService.h"
#import "NBKitConfig.h"
@import MessageUI;
@import Social;
@import Accounts;

static NSString *const kNBSMSColor          = @"6DB731";
static NSString *const kNBFacebookColor     = @"3D4D8B";
static NSString *const kNBTwitterColor      = @"5F87C3";
static NSString *const kNBWhatsAppColor     = @"6EB730";

@interface NBTellFriendViewController () <MFMessageComposeViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *messageLabel;

@property (weak, nonatomic) IBOutlet NBPillButton *SMSButton;
@property (weak, nonatomic) IBOutlet NBPillButton *facebookButton;
@property (weak, nonatomic) IBOutlet NBPillButton *twitterButton;
@property (weak, nonatomic) IBOutlet NBPillButton *moreButton;
@property (weak, nonatomic) IBOutlet UIButton *closeButton;

@end

@implementation NBTellFriendViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.messageLabel.text = [NBKitConfig shared].tellFriendPromptMessage;
    
    [self styleViews];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)styleViews
{
    for (UIView *v in self.view.subviews) {
        v.tintColor = nil;
    }
    
    self.view.tintColor = [UIColor whiteColor];
    
    self.titleLabel.font = [UIFont nb_alertTitleFontWithSize:40.0f];
    self.titleLabel.textColor = [UIColor whiteColor];
    
    self.messageLabel.font = [UIFont nb_alertMessageFontWithSize:18.0f];
    self.messageLabel.textColor = [UIColor whiteColor];
    
    UIImage *closeImage = self.closeButton.imageView.image;
    [self.closeButton setImage:[closeImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    
    [self styleFillButton:self.SMSButton color:kNBSMSColor icon:@"tf-chat-selected"];
    [self styleFillButton:self.facebookButton color:kNBFacebookColor icon:@"tf-facebook-selected"];
    [self styleFillButton:self.twitterButton color:kNBTwitterColor icon:@"tf-twitter-selected"];
    [self styleFillButton:self.moreButton color:kNBWhatsAppColor icon:@"tf-share-selected"];
}

- (void)styleFillButton:(NBPillButton *)button color:(NSString *)color icon:(NSString *)icon
{
    button.titleLabel.font = [UIFont nb_alertButtonFontWithSize:20];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button.pillFillColor = [UIColor rz_colorFromHexString:color];
    button.highlightedFillColor = [button.pillFillColor nb_colorAdjustedByPercent:0.75];

    button.tintColor = [UIColor whiteColor];
    UIImage *image = [UIImage imageNamed:icon];
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate]];
    imageView.contentMode = UIViewContentModeScaleAspectFit;;
    [button addSubview:imageView];
    
    imageView.translatesAutoresizingMaskIntoConstraints = NO;
    CGFloat s = button.bounds.size.height - 10.0f;
    [imageView rz_pinSizeTo:CGSizeMake(s, s)];
    [imageView rz_centerVerticallyInContainer];
    [imageView rz_pinLeftSpaceToSuperviewWithPadding:10.0f];
}

#pragma mark - Actions

- (IBAction)cancelTapped:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - SMS

- (IBAction)SMSTapped:(id)sender
{
    if ([MFMessageComposeViewController canSendText]) {
        MFMessageComposeViewController *messageVC = [[MFMessageComposeViewController alloc] init];
        messageVC.messageComposeDelegate = self;
        messageVC.body = [NSString stringWithFormat:@"%@ %@", [NBKitConfig shared].tellFriendMessage, [self shareURL]];
        messageVC.modalPresentationStyle = UIModalPresentationFormSheet;
        
        [self presentViewController:messageVC animated:YES completion:nil];
    }
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result;
{
    if (result == MessageComposeResultSent) {
        [controller dismissViewControllerAnimated:YES completion:^{
            [self showThanks];
        }];
    }
    else {
        [controller dismissViewControllerAnimated:YES completion:nil];
    }
}

#pragma mark - Social

- (IBAction)facebookTapped:(id)sender
{
    [self doSocialComposeWithType:SLServiceTypeFacebook];
}

- (IBAction)twitterTapped:(id)sender
{
    [self doSocialComposeWithType:SLServiceTypeTwitter];
}

- (IBAction)moreTapped:(id)sender
{
    UIActivityViewController* avc = [[UIActivityViewController alloc] initWithActivityItems:@[ [NBKitConfig shared].tellFriendMessage, [self shareURL]]
                                                                      applicationActivities:nil];
    avc.excludedActivityTypes = @[ UIActivityTypePostToWeibo, UIActivityTypeCopyToPasteboard ];
    
    avc.completionHandler = ^(NSString *activityType, BOOL completed){
        if(completed) {
            [self showThanks];
        }
    };
    
    avc.popoverPresentationController.sourceView = self.view;
    avc.popoverPresentationController.sourceRect = self.moreButton.frame;
    
    [self presentViewController:avc animated:YES completion:nil];
}

#pragma mark - Helper

- (void)doSocialComposeWithType:(NSString *)type
{
    if ([SLComposeViewController isAvailableForServiceType:type]) {
        
        SLComposeViewController *composeVC = [SLComposeViewController composeViewControllerForServiceType:type];
        [composeVC setInitialText:[NBKitConfig shared].tellFriendMessage];
        [composeVC addURL:[NSURL URLWithString:[self shareURL]]];
        composeVC.modalPresentationStyle = UIModalPresentationFormSheet;
        
        [composeVC setCompletionHandler:^(SLComposeViewControllerResult result) {
            if(result == SLComposeViewControllerResultDone) {
                [self showThanks];
            }
        }];
        
        [self presentViewController:composeVC animated:YES completion:nil];
    }
}

- (NSString *)shareURL
{
    return [[NBKitConfig shared] appURLWithAffiliateCampaign:@"tell_friend"];
}

- (void)showThanks
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Thanks!", nil) message:NSLocalizedString(@"Thanks for telling your friends!", nil) preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Dismiss", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self finishActionAndDismiss];
    }]];
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)finishActionAndDismiss
{
    [[NSNotificationCenter defaultCenter] postNotificationName:NBSharingServiceDidTellFriend object:self];
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
