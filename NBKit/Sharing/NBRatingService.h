//
//  NBRatingService.h
//  NBKit
//
//  Created by Nicholas Bonatsakis on 8/9/14.
//  Copyright (c) 2014 Atlantia Software LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

OBJC_EXTERN NSString *const NBRatingServiceDidRateNotification;

@interface NBRatingService : NSObject

+ (NBRatingService *)shared;
- (void)showPrompt;

@end
