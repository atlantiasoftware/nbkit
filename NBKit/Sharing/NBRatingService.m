//
//  NBRatingService.m
//  NBKit
//
//  Created by Nicholas Bonatsakis on 8/9/14.
//  Copyright (c) 2014 Atlantia Software LLC. All rights reserved.
//

#import "NBRatingService.h"
#import "iRate.h"
#import "NBRateViewController.h"
#import "NBKitConfig.h"
#import "NBShrinkBlurAnimationController.h"

NSString *const NBRatingServiceDidRateNotification = @"com.nbkit.NBRatingServiceDidRateNotification";


@interface NBRatingService () <iRateDelegate, UIViewControllerTransitioningDelegate>

@property (strong, nonatomic) NBShrinkBlurAnimationController *blurTransition;
@property (strong, nonatomic) UIViewController *presentedRateVC;

@end

@implementation NBRatingService

+ (NBRatingService *)shared
{
    static NBRatingService *s = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        s = [[NBRatingService alloc] init];
    });
    
    return s;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        [iRate sharedInstance].delegate = self;
        [iRate sharedInstance].daysUntilPrompt = 5;
        [iRate sharedInstance].usesUntilPrompt = 2;
        [iRate sharedInstance].promptAtLaunch = YES;
        [iRate sharedInstance].onlyPromptIfLatestVersion = NO;

        self.blurTransition = [[NBShrinkBlurAnimationController alloc] init];
        self.blurTransition.blurEffectStyle = UIBlurEffectStyleDark;
    }
    return self;
}

- (void)iRateDidOpenAppStore
{
}

- (void)iRateCouldNotConnectToAppStore:(NSError *)error
{
    if ( [iRate sharedInstance].ratingsURL && [[UIApplication sharedApplication] canOpenURL:[iRate sharedInstance].ratingsURL] ) {
        [[UIApplication sharedApplication] openURL:[iRate sharedInstance].ratingsURL];
    }
    else if ( [[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:[NBKitConfig shared].appURL]] ) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NBKitConfig shared].appURL]];
    }
    
    [self.presentedRateVC dismissViewControllerAnimated:YES completion:^{
        self.presentedRateVC = nil;
    }];
}

- (BOOL)iRateShouldPromptForRating
{
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self showPrompt];
    });
    
    return NO;
}

- (void)showPrompt
{
    NBRateViewController *rateVC = [[NBRateViewController alloc] initWithNibName:NSStringFromClass([NBRateViewController class]) bundle:nil];
    rateVC.transitioningDelegate = self;
    UIViewController *targetVC = [self topViewController:[UIApplication sharedApplication].keyWindow.rootViewController];
    [targetVC presentViewController:rateVC animated:YES completion:nil];

    self.presentedRateVC = rateVC;
}

- (UIViewController *)topViewController:(UIViewController *)viewController
{
    if (viewController.presentedViewController != nil) {
        return [self topViewController:viewController.presentedViewController];
    }
    else {
        return viewController;
    }
}

#pragma mark - Transition

- (id <UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source
{
    self.blurTransition.isPositiveAnimation = YES;
    return self.blurTransition;
}

- (id <UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed
{
    self.blurTransition.isPositiveAnimation = NO;
    return self.blurTransition;
}

@end
