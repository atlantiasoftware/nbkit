//
//  NBKitConfig.h
//  Jupiter
//
//  Created by Nicholas Bonatsakis on 2/15/14.
//
//

#import <Foundation/Foundation.h>

typedef void(^NBNoArgBlock)(void);

@interface NBKitConfig : NSObject

@property (nonatomic, assign) BOOL burstlyEnabeld;
@property (nonatomic, assign) BOOL facebookEnabled;

#pragma mark - General

@property (nonatomic, copy) NSString *appID;
@property (nonatomic, readonly) NSString *appURL;
@property (nonatomic, copy) NSString *feedbackEmailAddress;
@property (nonatomic, copy) NSString *appDisplayName;
@property (nonatomic, copy) NSString *appTagline;
@property (nonatomic, copy) NSString *appShortLink;
@property (nonatomic, copy) NSString *appShortName;

- (NSString *)appURLWithAffiliateCampaign:(NSString *)campaign;
- (NSString *)URLForAppWithID:(NSString *)appID campaign:(NSString *)campaign;

#pragma mark - Analytics

@property (nonatomic, copy) NSString *flurryAPIKey;
@property (nonatomic, copy) NSString *tapstreamAccountName;
@property (nonatomic, copy) NSString *tapstreamSecret;
@property (nonatomic, copy) NSString *googleTrackingID;

#pragma mark - Push

@property (nonatomic, copy) NSString *parseAppID;
@property (nonatomic, copy) NSString *parseClientKey;

#pragma mark - Themes

@property (strong, nonatomic) UIColor *appTintColor;

#pragma mark - RatingPrompt

@property (strong, nonatomic) NSString *ratingMessage;

#pragma mark - More Apps

@property (copy, nonatomic) NSString *moreAppsITunesID;
@property (copy, nonatomic) NSString *moreAppsAffiliateToken;
@property (copy, nonatomic) NSString *moreAppsCampaignToken;

#pragma mark - Sharing

@property (strong, nonatomic) NSString *tellFriendPromptMessage;
@property (copy, nonatomic) NSString *tellFriendMessage;

+ (NBKitConfig *)shared;

@end
