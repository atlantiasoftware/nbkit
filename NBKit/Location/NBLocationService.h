//
//  NBLocationService.h
//  Mozart
//
//  Created by nbonatsakis on 11/27/12.
//  Copyright (c) 2012 none. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

typedef void(^NBLocationServiceSuccessBlock)(id result);
typedef void(^NBLocationServiceErrorBlock)(NSError* error);

@interface NBLocationService : NSObject <CLLocationManagerDelegate>

@property (nonatomic, readonly) CLLocationManager* locationManager;
@property (nonatomic, readonly) CLGeocoder* geocoder;

@property (nonatomic, readonly, strong) CLLocation *lastLocation;
@property (nonatomic, readonly, strong) CLPlacemark *lastPlacemark;

+ (NBLocationService *)shared;

- (void) fetchLocationWithCompletion:(NBLocationServiceSuccessBlock)successBlock
                               error:(NBLocationServiceErrorBlock)errorBlock;
- (void) fetchPlacemarkAtCurrentLocationWithCompletion:(NBLocationServiceSuccessBlock)successBlock
                               error:(NBLocationServiceErrorBlock)errorBlock;

@end
