//
//  NBLocationService.m
//  Mozart
//
//  Created by nbonatsakis on 11/27/12.
//  Copyright (c) 2012 none. All rights reserved.
//

#import "NBLocationService.h"

#define kDefaultUpdateTimeout 3.0
#define kAcceptableAccuracyMeters 100

@interface NBLocationService ()

@property (nonatomic, strong) CLLocation* bestEffortAtLocation;
@property (nonatomic, copy) NSMutableArray* successBlocks;
@property (nonatomic, copy) NSMutableArray* errorBlocks;
@property (nonatomic, copy) NBLocationServiceSuccessBlock geoSuccessBlock;
@property (nonatomic, copy) NBLocationServiceErrorBlock geoErrorBlock;
@property (nonatomic) int updateAtempts;
@property (nonatomic) BOOL fetchLocInProgress;
@property (nonatomic, readwrite, strong) CLLocation *lastLocation;
@property (nonatomic, readwrite, strong) CLPlacemark *lastPlacemark;

@end

@implementation NBLocationService

+ (NBLocationService *)shared
{
    static NBLocationService *shared = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        shared = [[NBLocationService alloc] init];
    });
    
    return shared;
}

- (id)init {
    self = [super init];
    if (self) {
        _locationManager = [[CLLocationManager alloc] init];
        _locationManager.delegate = self;
        _geocoder = [[CLGeocoder alloc] init];
        _successBlocks = [NSMutableArray array];
        _errorBlocks = [NSMutableArray array];
        _fetchLocInProgress = NO;
    }
    return self;
}

- (void) setUpDefaults {
    _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
}

- (void) fetchLocationWithCompletion:(NBLocationServiceSuccessBlock)successBlock
                               error:(NBLocationServiceErrorBlock)errorBlock {
    
    if (![CLLocationManager locationServicesEnabled]) {
        NSError *err = [NSError errorWithDomain:@"CLLocationErrorDomain" code:1 userInfo:@{ NSLocalizedDescriptionKey : @"Location services are not enabled"}];
        errorBlock(err);
        return;
    }
    
    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusNotDetermined ) {
        [self.locationManager requestWhenInUseAuthorization];
    }
    
    //CLSLOG(@"Fetching location");
    [self.successBlocks addObject:successBlock];
    [self.errorBlocks addObject:errorBlock];
    if (self.fetchLocInProgress) {
        return;
    }
    
    self.updateAtempts = 0;
    self.fetchLocInProgress = YES;
    self.bestEffortAtLocation = nil;
    [self.locationManager startUpdatingLocation];
    
    // Only schedule the timer if we are NOT waiting for authorization
    if ([CLLocationManager authorizationStatus] != kCLAuthorizationStatusNotDetermined) {
        [self performSelector:@selector(locationUpdateTimeout) withObject:nil afterDelay:kDefaultUpdateTimeout];
    }
}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    // if the status is Authorized and we were waiting for authorization, start the timeout timer.
    if (status == kCLAuthorizationStatusAuthorized && self.fetchLocInProgress) {
        [self performSelector:@selector(locationUpdateTimeout) withObject:nil afterDelay:kDefaultUpdateTimeout];
    }
}

- (void) resetBlocks {
    [self.successBlocks removeAllObjects];
    [self.errorBlocks removeAllObjects];
}

#pragma mark - Location Delegate

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    //CLSLOG(@"Did update location");
    self.updateAtempts++;
    CLLocation* newLocation = [locations lastObject];
    
    // test the age of the location measurement to determine if the measurement is cached
    // in most cases you will not want to rely on cached measurements
    NSTimeInterval locationAge = -[newLocation.timestamp timeIntervalSinceNow];
    if (locationAge > 5.0) {
        //CLSLOG(@"Age too old:%f", locationAge);
        return;
    }
    
    // test that the horizontal accuracy does not indicate an invalid measurement
    if (newLocation.horizontalAccuracy < 0) {
        //CLSLOG(@"Invalid horizontal accuracy:%f", newLocation.horizontalAccuracy);
        return;
    }
    
    // test the measurement to see if it is more accurate than the previous measurement
    if (self.bestEffortAtLocation == nil || self.bestEffortAtLocation.horizontalAccuracy > newLocation.horizontalAccuracy) {
        if (!self.bestEffortAtLocation) {
            //CLSLOG(@"No existing bestEffort, storingAcc:%f", newLocation.horizontalAccuracy);
        } else {
            //CLSLOG(@"Store best effort. bestEffortAcc:%f newLocationAcc:%f",
            //                  self.bestEffortAtLocation.horizontalAccuracy, newLocation.horizontalAccuracy);
        }
        
        // store the location as the "best effort"
        self.bestEffortAtLocation = newLocation;
        // test the measurement to see if it meets the desired accuracy
        //
        // IMPORTANT!!! kCLLocationAccuracyBest should not be used for comparison with location coordinate or altitidue
        // accuracy because it is a negative value. Instead, compare against some predetermined "real" measure of
        // acceptable accuracy, or depend on the timeout to stop updating. This sample depends on the timeout.
        //
        double acceptable = kAcceptableAccuracyMeters;
        if (newLocation.horizontalAccuracy <= acceptable) {
            //CLSLOG(@"Accept location, atempts:%i Accuracy %f < %f", self.updateAtempts, newLocation.horizontalAccuracy, acceptable);
            [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(locationUpdateTimeout) object:nil];
            [self.locationManager stopUpdatingLocation];
            for (NBLocationServiceSuccessBlock b in self.successBlocks) {
                b(newLocation);
            }
            self.lastLocation = newLocation;
            self.fetchLocInProgress = NO;
            [self resetBlocks];
        }
    } else {
        //CLSLOG(@"Discard newLocationAcc:%f", newLocation.horizontalAccuracy);
    }
}

- (void) locationUpdateTimeout {
    // if we have some location that is not expired, we'll settle on it and use it.
    if (self.bestEffortAtLocation) {
        //CLSLOG(@"Timeout, atempts:%i. Accept bestEffortAcc:%f", self.updateAtempts, self.bestEffortAtLocation.horizontalAccuracy);
        [self.locationManager stopUpdatingLocation];
        for (NBLocationServiceSuccessBlock b in self.successBlocks) {
            b(self.bestEffortAtLocation);
        }
        self.lastLocation = self.bestEffortAtLocation;
        self.fetchLocInProgress = NO;
        [self resetBlocks];
    } else {
        //CLSLOG(@"Timeout. Error, no bestEffort");
        NSError* error = [NSError errorWithDomain:@"NBLocationServiceErrorDomain" code:1
                                         userInfo:@{NSLocalizedDescriptionKey : @"Timed out waiting for location."}];
        [self locationManager:self.locationManager didFailWithError:error];
    }
}

- (void) locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    // The location "unknown" error simply means the manager is currently unable to get the location.
    // We can ignore this error for the scenario of getting a single location fix, because we already have a
    // timeout that will stop the location manager to save power.
    if ([error code] != kCLErrorLocationUnknown) {
        [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(locationUpdateTimeout) object:nil];
        [self.locationManager stopUpdatingLocation];
        for (NBLocationServiceErrorBlock b in self.errorBlocks) {
            b(error);
        }
        self.fetchLocInProgress = NO;
        [self resetBlocks];
    }
}

#pragma mark - Geocoding

- (void) fetchPlacemarkAtCurrentLocationWithCompletion:(NBLocationServiceSuccessBlock)successBlock
                                                 error:(NBLocationServiceErrorBlock)errorBlock {
    self.geoSuccessBlock = successBlock;
    self.geoErrorBlock = errorBlock;
    
    __weak NBLocationService *weakSelf = self;
    [self fetchLocationWithCompletion:^(id result) {
        [weakSelf.geocoder reverseGeocodeLocation:result
                                completionHandler:^(NSArray *placemarks, NSError *error) {
                                    if (error && weakSelf.geoErrorBlock) {
                                        weakSelf.geoErrorBlock(error);
                                    } else if (weakSelf.geoSuccessBlock) {
                                        weakSelf.lastPlacemark = placemarks[0];
                                        weakSelf.geoSuccessBlock(placemarks[0]);
                                    }
                                }];
    } error:^(NSError *error) {
        if (weakSelf.geoErrorBlock) weakSelf.geoErrorBlock(error);
    }];
}

@end
