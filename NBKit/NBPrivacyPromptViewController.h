//
//  NBPrivacyPromptViewController.h
//  NoPhone
//
//  Created by Nicholas Bonatsakis on 1/19/15.
//  Copyright (c) 2015 Atlantia Software LLC. All rights reserved.
//

@import Foundation;
@import UIKit;

@interface NBPromptStyle : NSObject
@property (strong, nonatomic) UIFont *titleFont;
@property (strong, nonatomic) UIFont *messageFont;
@property (strong, nonatomic) UIFont *acceptButtonFont;
@property (strong, nonatomic) UIFont *declineButtonFont;
@property (strong, nonatomic) UIColor *foregroundColor;
@property (strong, nonatomic) UIColor *backgroundColor;
@property (strong, nonatomic) UIColor *tintColor;
@end

typedef void(^NBPrivacyPromptAction)(void);

@interface NBPrivacyPromptViewController : UIViewController

@property (strong, readonly, nonatomic) NBPromptStyle *style;
@property (copy, nonatomic) NSString *message;
@property (copy, nonatomic) NSString *declineButtonTitle;
@property (copy, nonatomic) NSString *acceptButtonTitle;
@property (strong, nonatomic) UIImage *image;

@property (copy, nonatomic) NBPrivacyPromptAction declineAction;
@property (copy, nonatomic) NBPrivacyPromptAction acceptAction;


+ (BOOL)hasAllowedNotifications;
+ (BOOL)hasPromptedForNotifications;
+ (void)markHasPromptedForNotifications;

@end
