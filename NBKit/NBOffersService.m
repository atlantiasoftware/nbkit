//
//  TTOffersService.m
//  Jupiter
//
//  Created by Nicholas Bonatsakis on 11/4/14.
//
//

#import "NBOffersService.h"
#import "NBRatingService.h"
#import "NBStoreFront.h"
#import "NBKitconfig.h"
#import "NBRatingService.h"
#import "NBSharingService.h"

NSString *const kNBOffersServiceRateOfferID       = @"com.atlantia.RateOfferAction";
NSString *const kNBOffersServiceDidRateAppID      = @"com.atlantia.DidRate";

NSString *const kNBOffersServiceTellFriendOfferID       = @"com.atlantia.TellFriendOfferAction";
NSString *const kNBOffersServiceDidTellFriendID      = @"com.atlantia.DidTellFriend";

@interface NBOffersService ()

@end

@implementation NBOffersService

+ (NBOffersService *)shared
{
    static NBOffersService *s = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        s = [[NBOffersService alloc] init];
    });
    
    return s;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        _rateOfferAfterDays = 3.0;
        _tellFriendAfterDays = 4.0;
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleDidRate:) name:NBRatingServiceDidRateNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleDidTellFriend:) name:NBSharingServiceDidTellFriend object:nil];
    }
    return self;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)registerRatingOfferWithMessage:(NSString *)message
{
}

#pragma mark - Rate App

- (void)configureRateAppWithMessage:(NSString *)message
{
    [NBRatingService shared];    
    
    if (message != nil) {
        [NBKitConfig shared].ratingMessage = message;
    }
}

- (void)handleDidRate:(NSNotification *)notification
{
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kNBOffersServiceDidRateAppID];
    [[NSUserDefaults standardUserDefaults] synchronize];

    if (self.didRateAction != nil) {
        self.didRateAction();
    }
}

- (BOOL)didRateApp
{
    return [[NSUserDefaults standardUserDefaults] boolForKey:kNBOffersServiceDidRateAppID];
}

#pragma mark - Tell Friend

- (void)registerTellFriendOfferWithMessage:(NSString *)message
{
}

- (void)handleDidTellFriend:(NSNotification *)notification
{
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kNBOffersServiceDidTellFriendID];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    if (self.tellFriendAction != nil) {
        self.tellFriendAction();
    }
}

- (BOOL)didTellFriend
{
    return [[NSUserDefaults standardUserDefaults] boolForKey:kNBOffersServiceDidTellFriendID];
}

@end
