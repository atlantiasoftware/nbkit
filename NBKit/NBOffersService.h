//
//  TTOffersService.h
//  Jupiter
//
//  Created by Nicholas Bonatsakis on 11/4/14.
//
//

@import Foundation;

typedef void(^NBOffersServiceCompletion)(void);

@interface NBOffersService : NSObject

@property (copy, nonatomic) NBOffersServiceCompletion didRateAction;
@property (assign, nonatomic) NSTimeInterval rateOfferAfterDays;
@property (readonly) BOOL didRateApp;

@property (copy, nonatomic) NBOffersServiceCompletion tellFriendAction;
@property (assign, nonatomic) NSTimeInterval tellFriendAfterDays;
@property (readonly) BOOL didTellFriend;

+ (NBOffersService *)shared;

- (void)registerRatingOfferWithMessage:(NSString *)message;
- (void)configureRateAppWithMessage:(NSString *)message;

- (void)registerTellFriendOfferWithMessage:(NSString *)message;

@end
