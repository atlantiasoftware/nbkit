//
//  NBMockProduct.m
//  InstaConcert
//
//  Created by Nicholas Bonatsakis on 11/3/13.
//  Copyright (c) 2013 Atlantia Software. All rights reserved.
//

#import "NBMockProduct.h"

@implementation NBMockProduct
{
    NSString *_identifier;
    NSString *_mockTitle;
    NSString *_mockDesc;
    NSDecimalNumber *_mockPrice;
    NSLocale *_mockLocale;
}


- (instancetype)initWithIdentifier:(NSString *)identifier title:(NSString *)title desc:(NSString *)desc price:(NSDecimalNumber *)price locale:(NSLocale *)locale
{
    self = [super init];
    if (self) {
        _identifier = identifier;
        _mockTitle = title;
        _mockDesc = desc;
        _mockPrice = price;
        _mockLocale = locale;
    }
    return self;
}

- (NSString *)productIdentifier
{
    return _identifier;
}

- (NSString *)localizedTitle
{
    return _mockTitle;
}

- (NSString *)localizedDescription
{
    return _mockDesc;
}

- (NSDecimalNumber *)price
{
    return _mockPrice;
}

- (NSLocale *)priceLocale
{
    return _mockLocale;
}

@end
