//
//  NBStoreFront.h
//  InstaConcert
//
//  Created by Nicholas Bonatsakis on 10/31/13.
//  Copyright (c) 2013 Atlantia Software. All rights reserved.
//

#import <Foundation/Foundation.h>
@import StoreKit;

#define NBSK_TEST 0

OBJC_EXTERN NSString *const kNBStoreFrontPurchaseCompleteKey;
OBJC_EXTERN NSString *const kNBStoreFrontPurchaseCompleteProductIDKey;
OBJC_EXTERN NSString *const kNBStoreFrontOfferCompleteKey;
OBJC_EXTERN NSString *const kNBStoreFrontOfferCompleteOfferKey;

@class NBStoreFrontStyle;

typedef void(^NBStoreFrontAppStoreVersionBlock)(BOOL isCurrentVersionLive);

@interface NBStoreFront : NSObject

+ (NBStoreFront *)shared;

- (void)configureWithProductIdentifiers:(NSArray *)identifiers;
- (void)requestProducts:(NSSet*)identifiers
                success:(void (^)(NSArray *products, NSArray *invalidProductIdentifiers))successBlock
                failure:(void (^)(NSError *error))failureBlock;

- (void)addPayment:(NSString*)productIdentifier
           success:(void (^)(SKPaymentTransaction *transaction))successBlock
           failure:(void (^)(SKPaymentTransaction *transaction, NSError *error))failureBlock;

- (void)restoreTransactionsOnSuccess:(void (^)())successBlock
                             failure:(void (^)(NSError *error))failureBlock;

- (BOOL)isFeatureAvailable:(NSString *)productID;
- (BOOL)hasMadePurchase;

- (UIViewController *)presentStoreFrontInViewController:(UIViewController *)presentInvVC;
- (UIViewController *)presentStoreFrontInViewController:(UIViewController *)presentInvVC identifiers:(NSArray *)identifiers;

- (void)presentMoreAppsFromViewController:(UIViewController *)viewController;

+ (NBStoreFrontStyle *)style;

#pragma mark - Offers

@property (assign, nonatomic) BOOL offersEnabled;
@property (copy, nonatomic) NSArray *offers;
- (void)completeOfferForID:(NSString *)offerID;

#pragma mark - App Store

- (void)checkIsCurrentVersionLive:(NBStoreFrontAppStoreVersionBlock)completion;

@end
