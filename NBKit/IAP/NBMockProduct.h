//
//  NBMockProduct.h
//  InstaConcert
//
//  Created by Nicholas Bonatsakis on 11/3/13.
//  Copyright (c) 2013 Atlantia Software. All rights reserved.
//

@import StoreKit;

@interface NBMockProduct : SKProduct

- (instancetype)initWithIdentifier:(NSString *)identifier title:(NSString *)title desc:(NSString *)desc price:(NSDecimalNumber *)price locale:(NSLocale *)locale;

@end
