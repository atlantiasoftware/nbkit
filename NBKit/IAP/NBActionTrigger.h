//
//  NBActionTrigger.h
//  TotallyTabata
//
//  Created by Nicholas Bonatsakis on 4/6/14.
//
//

#import <Foundation/Foundation.h>

typedef void(^NBActionTriggerBlock)(void);

@interface NBActionTrigger : NSObject

+ (void)markAction:(NSString *)action
afterNumberOfTimes:(NSInteger)times
           doBlock:(NBActionTriggerBlock)actionBlock;

@end
