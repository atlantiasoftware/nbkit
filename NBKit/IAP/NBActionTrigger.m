//
//  NBActionTrigger.m
//  TotallyTabata
//
//  Created by Nicholas Bonatsakis on 4/6/14.
//
//

#import "NBActionTrigger.h"

@implementation NBActionTrigger

+ (void)setTimes:(NSInteger)times forAction:(NSString *)action
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:@(times) forKey:action];

    NSString *countStr = [NSString stringWithFormat:@"%@_count", action];
    if ([defaults objectForKey:countStr] == nil) {
        [defaults setObject:@0 forKey:countStr];
    }
}

+ (void)markAction:(NSString *)action
afterNumberOfTimes:(NSInteger)times
           doBlock:(NBActionTriggerBlock)actionBlock
{
    [self setTimes:times forAction:action];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSString *countStr = [NSString stringWithFormat:@"%@_count", action];
    
    NSNumber *max = [defaults objectForKey:action];
    NSNumber *count = [defaults objectForKey:countStr];
    if (max != nil && count != nil) {
        NSInteger maxInt = [max integerValue];
        NSInteger countInt = [count integerValue];
        NSInteger next = 0;
        
        if (countInt == maxInt) {
            actionBlock();
        } else {
            next = countInt + 1;
        }
        
        [defaults setObject:@(next) forKey:countStr];
    }
}

@end
