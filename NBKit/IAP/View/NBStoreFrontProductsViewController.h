//
//  NBStoreFrontProductsViewController.h
//  InstaConcert
//
//  Created by Nicholas Bonatsakis on 10/31/13.
//  Copyright (c) 2013 Atlantia Software. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NBStoreFrontProductsViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

- (instancetype)initWithProductIdentifiers:(NSArray *)identifiers;

@end
