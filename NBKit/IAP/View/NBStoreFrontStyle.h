//
//  NBStoreFrontStyle.h
//  NBKit
//
//  Created by Nicholas Bonatsakis on 4/16/14.
//  Copyright (c) 2014 Atlantia Software LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, NBStoreFrontStyleContentStyle) {
    NBStoreFrontStyleContentStyleLight = 0,
    NBStoreFrontStyleContentStyleDark
};

@interface NBStoreFrontStyle : NSObject

@property (nonatomic, strong) UIColor *storeBackgroundColor;
@property (nonatomic, strong) UIColor *storeForegroundColor;
@property (nonatomic, copy) NSString *storeHeaderFontName;
@property (nonatomic, copy) NSString *storeBodyFontName;
@property (nonatomic, strong) UIColor *storeBuyButtonTintColor;
@property (nonatomic, assign) NBStoreFrontStyleContentStyle contentStyle;

@end
