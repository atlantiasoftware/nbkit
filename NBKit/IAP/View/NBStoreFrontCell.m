//
//  NBStoreFrontCell.m
//  InstaConcert
//
//  Created by Nicholas Bonatsakis on 11/1/13.
//  Copyright (c) 2013 Atlantia Software. All rights reserved.
//

#import "NBStoreFrontCell.h"

@import StoreKit;
@import QuartzCore;

#import "NBStoreFront.h"
#import "NBStoreFrontStyle.h"
#import "NBStoreOffer.h"
#import "NBAppearance.h"

@interface NBStoreFrontCell ()

@property (nonatomic, weak) SKProduct *productObj;
@property (nonatomic, weak) NBStoreOffer *offerObj;
@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UILabel *detailLabel;
@property (nonatomic, weak) IBOutlet UIButton *buyButton;
@property (nonatomic, weak) IBOutlet UIView *separatorView;

@end

@implementation NBStoreFrontCell

- (void)awakeFromNib
{
    NBStoreFrontStyle *style = [NBStoreFront style];
    self.backgroundColor = style.storeBackgroundColor;
    
    self.titleLabel.font = [UIFont fontWithName:style.storeHeaderFontName size:self.titleLabel.font.pointSize];
    self.titleLabel.textColor = style.storeForegroundColor;
    self.detailLabel.font = [UIFont fontWithName:style.storeBodyFontName size:self.detailLabel.font.pointSize];
    self.detailLabel.textColor = style.storeForegroundColor;
    
    self.buyButton.layer.borderWidth = 1.0f;
    self.buyButton.tintColor = style.storeBuyButtonTintColor;
    self.buyButton.layer.borderColor = style.storeBuyButtonTintColor.CGColor;

    self.separatorView.backgroundColor = [UIColor clearColor];
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    
    self.titleLabel.text = nil;
    self.detailLabel.text = nil;
    [self.buyButton setTitle:nil forState:UIControlStateNormal];
    self.product = nil;
}

- (void)setProduct:(SKProduct *)product
{
    self.titleLabel.text = product.localizedTitle;
    self.detailLabel.text = product.localizedDescription;
    [self.buyButton setTitle:[self priceStringFromProduct:product] forState:UIControlStateNormal];
    self.productObj = product;    
}

- (void)setOffer:(NBStoreOffer *)offer
{
    self.titleLabel.text = offer.title;
    self.detailLabel.text = offer.desc;
    self.offerObj = offer;
    [self.buyButton setTitle:offer.actionTitle forState:UIControlStateNormal];
}

- (NSString *)priceStringFromProduct:(SKProduct *)product
{
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setLocale:product.priceLocale];
    [numberFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    return [numberFormatter stringFromNumber:product.price];
}

- (IBAction)buyTapped:(id)sender
{
    if (self.productObj != nil) {
        [self.delegate storeFrontCellDidSelectProduct:self.productObj];
    }
    
    if (self.offerObj != nil) {
        [self.delegate storeFrontCellDidSelectOffer:self.offerObj];
    }
}

@end
