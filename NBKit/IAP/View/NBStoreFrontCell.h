//
//  NBStoreFrontCell.h
//  InstaConcert
//
//  Created by Nicholas Bonatsakis on 11/1/13.
//  Copyright (c) 2013 Atlantia Software. All rights reserved.
//

#import <UIKit/UIKit.h>
@import StoreKit;

@class NBStoreOffer;

@protocol NBStoreFrontCellDelegate <NSObject>

- (void)storeFrontCellDidSelectProduct:(SKProduct *)product;
- (void)storeFrontCellDidSelectOffer:(NBStoreOffer *)offer;

@end

@interface NBStoreFrontCell : UITableViewCell

@property (nonatomic, weak) id<NBStoreFrontCellDelegate> delegate;

- (void)setProduct:(SKProduct *)product;
- (void)setOffer:(NBStoreOffer *)offer;

@end
