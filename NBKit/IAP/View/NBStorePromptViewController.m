//
//  NBStorePromptViewController.m
//  TotallyTabata
//
//  Created by Nicholas Bonatsakis on 4/6/14.
//
//

#import "NBStorePromptViewController.h"

#define kNBStorePromptBlurRadius            12.0f
#define kNBStorePromptBlurSaturationDelta   1.0f
#define kNBStorePromptBlurColor             [UIColor colorWithWhite:1.0f alpha:0.15f]

@interface NBStorePromptStyle : NSObject
@property (nonatomic, copy) NSString *primaryFontName;
@property (nonatomic, strong) UIColor *backgroundTintColor;
@end

@implementation NBStorePromptStyle
@end

@interface NBStorePromptViewController ()

@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UILabel *detailLabel;
@property (nonatomic, weak) IBOutlet UIButton *cancelButton;
@property (nonatomic, weak) IBOutlet UIButton *actionButton;
@property (nonatomic, weak) CAGradientLayer *gradientLayer;

@property (nonatomic, copy) NBStorePromptAction actionBlock;

@end

@implementation NBStorePromptViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    [self applyStyle];
    
    self.titleLabel.text = self.titleText;
    self.detailLabel.text = self.detailText;
    [self.actionButton setTitle:self.actionText forState:UIControlStateNormal];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    if (self.gradientLayer == nil) {
        UIColor *upperColor = [NBStorePromptViewController sharedStyle].backgroundTintColor;
        CAGradientLayer *gLayer = [CAGradientLayer layer];
        gLayer.colors = @[
                          (id)upperColor.CGColor,
                          (id)[UIColor blackColor].CGColor
                          ];
        [self.view.layer insertSublayer:gLayer atIndex:0];
        self.gradientLayer = gLayer;
    }
    
    self.gradientLayer.frame = self.view.layer.bounds;
}

- (IBAction)cancelTapped:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)actionTapped:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:^{
        if (self.actionBlock) {
            self.actionBlock();
        }
    }];
}

#pragma mark - Presentation

+ (void)presentInViewController:(UIViewController *)viewController
                       title:(NSString *)titleStr
                      detail:(NSString *)detailStr
                 actionTitle:(NSString *)actionTitle
                      action:(NBStorePromptAction)action
{
    NBStorePromptViewController *promptVC = [[NBStorePromptViewController alloc] init];
    promptVC.titleText = titleStr;
    promptVC.detailText = detailStr;
    promptVC.actionText = actionTitle;
    promptVC.actionBlock = action;
    
    promptVC.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    
    [viewController presentViewController:promptVC animated:YES completion:nil];
}

#pragma mark - Style

- (void)applyStyle
{
    NBStorePromptStyle* styleObj = [NBStorePromptViewController sharedStyle];
    
    self.titleLabel.font = [UIFont fontWithName:styleObj.primaryFontName size:28.0f];
    self.titleLabel.textColor = [UIColor whiteColor];
    self.detailLabel.font = [UIFont fontWithName:styleObj.primaryFontName size:18.0f];
    self.detailLabel.textColor = [UIColor whiteColor];
    
    self.actionButton.titleLabel.font = [UIFont fontWithName:styleObj.primaryFontName size:26.0f];
    [self.actionButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.actionButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
    self.actionButton.layer.borderColor = [UIColor whiteColor].CGColor;
    self.actionButton.layer.borderWidth = 1.0f;
    self.actionButton.layer.cornerRadius = self.actionButton.bounds.size.height / 2;
    self.actionButton.layer.contentsScale = [UIScreen mainScreen].scale;
    self.actionButton.backgroundColor = [UIColor clearColor];
}

+ (NBStorePromptStyle *)sharedStyle
{
    static NBStorePromptStyle *s = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        s = [[NBStorePromptStyle alloc] init];
    });
    
    return s;
}

+ (void)setPrimaryFontName:(NSString *)fontName
{
    [NBStorePromptViewController sharedStyle].primaryFontName = fontName;
}

+ (void)setBackbroundTintColor:(UIColor *)bgTintColor
{
    [NBStorePromptViewController sharedStyle].backgroundTintColor = bgTintColor;
}

@end
