//
//  NBStoreFrontProductsViewController.m
//  InstaConcert
//
//  Created by Nicholas Bonatsakis on 10/31/13.
//  Copyright (c) 2013 Atlantia Software. All rights reserved.
//

#import "NBStoreFrontProductsViewController.h"
#import <SVProgressHUD/SVProgressHUD.h>
#import "NBStoreFrontCell.h"
#import "NBStoreFront.h"
@import StoreKit;
#import "NBKitConfig.h"
#import "NBStoreFrontStyle.h"
#import "NBStoreOffer.h"
#import "UIViewController+NBAlerts.h"
#import "NBStoreOffer.h"

#define kNBStoreFrontCellID @"storeFrontCellID"

typedef NS_ENUM(NSInteger, NBProductsSegment) {
    NBProductsSegmentPad,
    NBProductsSegmentOffers
};

@interface NBStoreFrontProductsViewController () <NBStoreFrontCellDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentedControl;
@property (nonatomic, copy) NSArray *productIdentifiers;
@property (nonatomic, copy) NSArray *products;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *verticalSegmentedControlConstraint;

@end

@implementation NBStoreFrontProductsViewController

- (instancetype)initWithProductIdentifiers:(NSArray *)identifiers
{
    self = [super init];
    if (self) {
        self.productIdentifiers = identifiers;
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleOfferComplete:) name:kNBStoreFrontOfferCompleteKey object:nil];
    }
    return self;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.edgesForExtendedLayout = UIRectEdgeNone;

    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([NBStoreFrontCell class]) bundle:nil] forCellReuseIdentifier:kNBStoreFrontCellID];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancelTapped:)];
    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
    
    NSString *name = [NBKitConfig shared].appDisplayName;
    self.navigationItem.title = [NSString stringWithFormat:@"%@ %@", name, NSLocalizedString(@"Store", nil)];
    
    NBStoreFrontStyle *style = [NBStoreFront style];
    self.tableView.backgroundColor = style.storeBackgroundColor;
    self.view.backgroundColor = style.storeBackgroundColor;
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 60.0f;

    [self styleSegmentedControl];
    
    if (![[NBStoreFront shared] offersEnabled]) {
        self.segmentedControl.hidden = YES;
        self.verticalSegmentedControlConstraint.constant = 20.0f;
    }
    
    [SVProgressHUD show];

    __weak NBStoreFrontProductsViewController *weakSelf = self;
    [[NBStoreFront shared] requestProducts:[NSSet setWithArray:self.productIdentifiers]
                                    success:^(NSArray *products, NSArray *invalidProductIdentifiers) {
                                        [SVProgressHUD dismiss];
                                        
                                        NSMutableArray *orderedProducts = [NSMutableArray array];
                                        for (NSString *productID in weakSelf.productIdentifiers) {
                                            NSArray *filtered = [products filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"productIdentifier == %@", productID]];
                                            if (filtered && filtered.count > 0 && ![[NBStoreFront shared] isFeatureAvailable:[filtered[0] productIdentifier]] ) {
                                                [orderedProducts addObject:filtered[0]];
                                            }
                                        }
                                        
                                        weakSelf.products = [orderedProducts copy];
                                        [weakSelf.tableView reloadData];
                                    } failure:^(NSError *error) {
                                        [SVProgressHUD dismiss];
                                        [[[UIAlertView alloc] initWithTitle:@"Problem"
                                                                    message:error.localizedDescription
                                                                   delegate:nil cancelButtonTitle:@"Dismiss"
                                                          otherButtonTitles:nil] show];
    }];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    self.tableView.contentInset = UIEdgeInsetsZero;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)styleSegmentedControl
{
    NBStoreFrontStyle *style = [NBStoreFront style];

    if (style.contentStyle == NBStoreFrontStyleContentStyleLight) {
        self.segmentedControl.tintColor = self.navigationController.navigationBar.barTintColor;
    }
    else {
        self.segmentedControl.tintColor = [UIColor darkGrayColor];
        [self.segmentedControl setTitleTextAttributes:@{
                                                        NSForegroundColorAttributeName: [UIColor whiteColor]
                                                        } forState:UIControlStateNormal];
        [self.segmentedControl setTitleTextAttributes:@{
                                                        NSForegroundColorAttributeName: [UIColor whiteColor]
                                                        } forState:UIControlStateSelected];
    }
}


#pragma mark - Actions

- (void)cancelTapped:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)segmentChanged:(id)sender
{
    [self.tableView reloadData];
}

#pragma mark - TableView

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.segmentedControl.selectedSegmentIndex == NBProductsSegmentPad) {
        return self.products.count;
    }
    else {
        return [NBStoreFront shared].offers.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NBStoreFrontCell *cell = (NBStoreFrontCell *) [self.tableView dequeueReusableCellWithIdentifier:kNBStoreFrontCellID];
    cell.tintColor = self.view.tintColor;

    if (self.segmentedControl.selectedSegmentIndex == NBProductsSegmentPad) {
        SKProduct *p = self.products[indexPath.row];
        [cell setProduct:p];
    }
    else {
        NBStoreOffer *offer = [NBStoreFront shared].offers[indexPath.row];
        [cell setOffer:offer];
    }
    
    
    cell.delegate = self;
    
    return cell;
}

#pragma mark - Store Cell Delegate

- (void)storeFrontCellDidSelectProduct:(SKProduct *)product;
{
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
    [[NBStoreFront shared] addPayment:product.productIdentifier
                              success:^(SKPaymentTransaction *transaction) {
                                  [SVProgressHUD dismiss];
                                  [self cancelTapped:nil];
                              } failure:^(SKPaymentTransaction *transaction, NSError *error) {
                                  [SVProgressHUD dismiss];
                                  [[[UIAlertView alloc] initWithTitle:@"Problem"
                                                              message:error.localizedDescription
                                                             delegate:nil cancelButtonTitle:@"Dismiss"
                                                    otherButtonTitles:nil] show];
                              }];
}

- (void)storeFrontCellDidSelectOffer:(NBStoreOffer *)offer
{
    if (offer.action != nil) {
        offer.action();
    }
}

#pragma  mark - Offers

- (void)handleOfferComplete:(NSNotification *)notification
{
    [self.tableView reloadData];
    NBStoreOffer *offer = notification.userInfo[kNBStoreFrontOfferCompleteOfferKey];
    [self nb_showAlertWithTitle:NSLocalizedString(@"Offer Unlocked", nil) body:[NSString stringWithFormat:NSLocalizedString(@"You just unlocked %@!", nil), offer.title]];
}

@end
