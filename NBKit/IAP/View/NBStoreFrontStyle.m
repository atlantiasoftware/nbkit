//
//  NBStoreFrontStyle.m
//  NBKit
//
//  Created by Nicholas Bonatsakis on 4/16/14.
//  Copyright (c) 2014 Atlantia Software LLC. All rights reserved.
//

#import "NBStoreFrontStyle.h"

@implementation NBStoreFrontStyle

- (instancetype)init
{
    self = [super init];
    if (self) {
        _storeBackgroundColor = [UIColor whiteColor];
        _storeForegroundColor = [UIColor blackColor];
        _storeBuyButtonTintColor = [UIColor blueColor];
    }
    return self;
}

@end
