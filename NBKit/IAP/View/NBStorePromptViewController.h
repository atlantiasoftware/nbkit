//
//  NBStorePromptViewController.h
//  TotallyTabata
//
//  Created by Nicholas Bonatsakis on 4/6/14.
//
//

#import <UIKit/UIKit.h>

typedef void(^NBStorePromptAction)(void);

@interface NBStorePromptViewController : UIViewController

@property (nonatomic, copy) NSString *titleText;
@property (nonatomic, copy) NSString *detailText;
@property (nonatomic, copy) NSString *actionText;
@property (nonatomic, strong) UIImage *backgroundImage;

+ (void)presentInViewController:(UIViewController *)viewController
                          title:(NSString *)titleStr
                         detail:(NSString *)detailStr
                    actionTitle:(NSString *)actionTitle
                         action:(NBStorePromptAction)action;

+ (void)setPrimaryFontName:(NSString *)fontName;
+ (void)setBackbroundTintColor:(UIColor *)bgTintColor;

@end
