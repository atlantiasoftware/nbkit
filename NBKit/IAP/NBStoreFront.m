//
//  NBStoreFront.m
//  InstaConcert
//
//  Created by Nicholas Bonatsakis on 10/31/13.
//  Copyright (c) 2013 Atlantia Software. All rights reserved.
//

#import "NBStoreFront.h"
#import "NBStoreFrontProductsViewController.h"
#import <RMStore/RMStore.h>
#import "NBMockProduct.h"
#import "NBKitConfig.h"
@import AdSupport;
#import "NBStoreFrontStyle.h"
#import "NBStoreOffer.h"
#import "SWFSemanticVersion.h"

NSString *const kNBStoreFrontPurchaseCompleteKey = @"NBStoreFrontPurchaseCompleteKey";
NSString *const kNBStoreFrontPurchaseCompleteProductIDKey = @"productID";
NSString *const kNBStoreFrontOfferCompleteKey = @"com.nbkit.NBStoreFrontOfferComplete";
NSString *const kNBStoreFrontOfferCompleteOfferKey = @"com.nbkit.NBStoreFrontOfferCompleteOffer";

@interface NBStoreFront () <RMStoreObserver, SKStoreProductViewControllerDelegate>

@property (nonatomic, copy) NSArray *productIdentifiers;

@end

@implementation NBStoreFront

+ (NBStoreFront *)shared
{
    static NBStoreFront *storeFront = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        if (storeFront == nil) {
            storeFront = [[NBStoreFront alloc] init];
            [[RMStore defaultStore] addStoreObserver:storeFront];
        }
    });
    
    return storeFront;
}

- (void)configureWithProductIdentifiers:(NSArray *)identifiers
{
    self.productIdentifiers = identifiers;
}

- (void)requestProducts:(NSSet*)identifiers
                success:(void (^)(NSArray *products, NSArray *invalidProductIdentifiers))successBlock
                failure:(void (^)(NSError *error))failureBlock
{
#if NBSK_TEST
    NSArray *products = @[
                          [[NBMockProduct alloc] initWithTitle:@"Premium Upgrade"
                                                          desc:@"Purchase to unlock app backgrounding, custom interval length, and remove all ads."
                                                         price:[NSDecimalNumber decimalNumberWithString:@"0.99"]
                                                        locale:[NSLocale currentLocale]]
                          ];
    
    double delayInSeconds = 4.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        successBlock(products, nil);
    });
    
#else
    [[RMStore defaultStore] requestProducts:identifiers success:successBlock failure:failureBlock];
#endif
}

- (void)addPayment:(NSString*)productIdentifier
           success:(void (^)(SKPaymentTransaction *transaction))successBlock
           failure:(void (^)(SKPaymentTransaction *transaction, NSError *error))failureBlock
{
#if NBSK_TEST
    double delayInSeconds = 4.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        successBlock(nil);
    });
#else
    [[RMStore defaultStore] addPayment:productIdentifier
                               success:^(SKPaymentTransaction *transaction) {
                                   SKProduct *p = [[RMStore defaultStore] productForIdentifier:productIdentifier];
                                   successBlock(transaction);
                               } failure:failureBlock];
#endif
}

- (void)restoreTransactionsOnSuccess:(void (^)())successBlock
                             failure:(void (^)(NSError *error))failureBlock
{
#if NBSK_TEST
    
    double delayInSeconds = 4.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        successBlock();
    });
    
#else
    
    [[RMStore defaultStore] restoreTransactionsOnSuccess:successBlock failure:failureBlock];
    
#endif
}

- (BOOL)isFeatureAvailable:(NSString *)productID
{
    //    if (YES) return YES;
    return [[NSUserDefaults standardUserDefaults] boolForKey:productID];
}

- (BOOL)hasMadePurchase
{
    for ( NSString *productID in self.productIdentifiers ) {
        if ( [self isFeatureAvailable:productID] ) {
            return YES;
        }
    }
    return NO;
}

#pragma mark - View

- (UIViewController *)presentStoreFrontInViewController:(UIViewController *)presentInvVC;
{
    return [self presentStoreFrontInViewController:presentInvVC identifiers:self.productIdentifiers];
}

- (UIViewController *)presentStoreFrontInViewController:(UIViewController *)presentInvVC identifiers:(NSArray *)identifiers
{
    NBStoreFrontProductsViewController *pvc = [[NBStoreFrontProductsViewController alloc] initWithProductIdentifiers:identifiers];
    UINavigationController *navVC = [[UINavigationController alloc] initWithRootViewController:pvc];
    navVC.view.tintColor = presentInvVC.view.tintColor;
    [presentInvVC presentViewController:navVC animated:YES completion:nil];
    return navVC;
}

#pragma mark - Observer

- (void)storePaymentTransactionFinished:(NSNotification*)notification
{
    NSString *productIdentifier = notification.rm_productIdentifier;
    
    NSRange range = [productIdentifier rangeOfString:@"_Sale"];
    if ( range.location != NSNotFound ) {
        productIdentifier = [productIdentifier substringToIndex:range.location];
    }
    
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:productIdentifier];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [[NSNotificationCenter defaultCenter] postNotificationName:kNBStoreFrontPurchaseCompleteKey object:self userInfo:@{kNBStoreFrontPurchaseCompleteProductIDKey : productIdentifier}];
}

#pragma mark - More Apps

- (void)presentMoreAppsFromViewController:(UIViewController *)viewController
{
    if ( [NBKitConfig shared].moreAppsITunesID != nil ) {
        SKStoreProductViewController *storeVC = [[SKStoreProductViewController alloc] init];
        storeVC.delegate = self;
        NSMutableDictionary *params = [@{ SKStoreProductParameterITunesItemIdentifier: [NBKitConfig shared].moreAppsITunesID } mutableCopy];

        if ( [NBKitConfig shared].moreAppsAffiliateToken != nil ) {
            params[SKStoreProductParameterAffiliateToken] = [NBKitConfig shared].moreAppsAffiliateToken;
        }
        
        if ( [NBKitConfig shared].moreAppsCampaignToken != nil ) {
            params[SKStoreProductParameterCampaignToken] = [NBKitConfig shared].moreAppsCampaignToken;
        }
        
        [storeVC loadProductWithParameters:params completionBlock:^(BOOL result, NSError *error) {
            
        }];
        
        [viewController presentViewController:storeVC animated:YES completion:nil];
    }
    
}

- (void)productViewControllerDidFinish:(SKStoreProductViewController *)viewController
{
    [viewController dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Style

+ (NBStoreFrontStyle *)style
{
    static NBStoreFrontStyle *s = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        s = [[NBStoreFrontStyle alloc] init];
    });
    
    return s;
}

#pragma mark - Offers

- (NSArray *)offers
{
    return [_offers filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"%K == NO", NSStringFromSelector(@selector(isUnlocked))]];
}

- (void)completeOfferForID:(NSString *)offerID
{
    NSArray *results = [_offers filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"%K == %@", NSStringFromSelector(@selector(identifier)), offerID]];
    if (results.count > 0) {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:offerID];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [[NSNotificationCenter defaultCenter] postNotificationName:kNBStoreFrontOfferCompleteKey object:self userInfo:@{kNBStoreFrontOfferCompleteOfferKey: results[0]}];
    }
}

#pragma mark - App Store

- (void)checkIsCurrentVersionLive:(NBStoreFrontAppStoreVersionBlock)completion
{
    NSString *appID = [NBKitConfig shared].appID;
    if (appID == nil) {
        completion(NO);
        return;
    }
    
    NSString *URLString = [NSString stringWithFormat:@"http://itunes.apple.com/us/lookup?id=%@", appID];
    NSString *version = [[NSBundle mainBundle] infoDictionary][@"CFBundleShortVersionString"];
    NSURLSessionDataTask *task = [[NSURLSession sharedSession] dataTaskWithURL:[NSURL URLWithString:URLString] completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if (error == nil) {
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            NSArray *results = json[@"results"];
            if (results.count > 0) {
                NSDictionary *appInfo = results[0];
                NSString *appStoreVersion = appInfo[@"version"];
                BOOL isLive = [self isLiveVersion:version appStoreVersion:appStoreVersion];
                completion(isLive);
            }
            
        } else {
            completion(NO);
        }
    }];
    
    [task resume];
}

- (BOOL)isLiveVersion:(NSString *)localVersion appStoreVersion:(NSString *)appStoreVersion
{
    SWFSemanticVersion *local = [SWFSemanticVersion semanticVersionWithString:localVersion];
    SWFSemanticVersion *appStore = [SWFSemanticVersion semanticVersionWithString:appStoreVersion];
    return [local compare:appStore] == NSOrderedAscending || [local compare:appStore] == NSOrderedSame;
}

@end
