//
//  NBStoreOffer.h
//  NBKit
//
//  Created by Nicholas Bonatsakis on 9/15/15.
//  Copyright © 2015 Atlantia Software LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

typedef void(^NBStoreOfferAction)(void);

@interface NBStoreOffer : NSObject

@property (copy, nonatomic) NSString *identifier;
@property (copy, nonatomic) NSString *title;
@property (copy, nonatomic) NSString *desc;
@property (copy, nonatomic) NSString *actionTitle;
@property (copy, nonatomic) NBStoreOfferAction action;
@property (readonly) BOOL isUnlocked;

- (instancetype)initWithIdentifier:(NSString *)identifier
                              title:(NSString *)title
                               desc:(NSString *)desc
                        actionTitle:(NSString *)actionTitle
                             action:(NBStoreOfferAction)action;

@end

NS_ASSUME_NONNULL_END