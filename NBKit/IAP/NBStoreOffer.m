//
//  NBStoreOffer.m
//  NBKit
//
//  Created by Nicholas Bonatsakis on 9/15/15.
//  Copyright © 2015 Atlantia Software LLC. All rights reserved.
//

#import "NBStoreOffer.h"

@implementation NBStoreOffer

- (instancetype)initWithIdentifier:(NSString *)identifier
                             title:(NSString *)title
                              desc:(NSString *)desc
                       actionTitle:(NSString *)actionTitle
                            action:(NBStoreOfferAction)action
{
    self = [super init];
    if (self) {
        _identifier = [identifier copy];
        _title = [title copy];
        _desc = [desc copy];
        _actionTitle = [actionTitle copy];
        _action = [action copy];
    }
    return self;
}

- (BOOL)isUnlocked
{
    return [[NSUserDefaults standardUserDefaults] boolForKey:self.identifier];
}

@end
