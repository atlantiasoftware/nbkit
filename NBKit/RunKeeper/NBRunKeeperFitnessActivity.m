//
//  NBRunKeeperFitnessActivity.m
//  Jupiter
//
//  Created by Nicholas Bonatsakis on 2/17/14.
//
//

#import "NBRunKeeperFitnessActivity.h"

@implementation NBRunKeeperFitnessActivity

- (NSDictionary *)toDict
{
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [self safeSetValue:self.type forKey:@"type" inDict:dict];
    [self safeSetValue:self.secondaryType forKey:@"secondary_type" inDict:dict];
    [self safeSetValue:self.notes forKey:@"notes" inDict:dict];
    [self safeSetValue:self.startDate forKey:@"start_time" inDict:dict];
    [self safeSetValue:self.durationSec forKey:@"duration" inDict:dict];
    [self safeSetValue:self.totalCalories forKey:@"total_calories" inDict:dict];
    [self safeSetValue:self.postToTwitter forKey:@"post_to_twitter" inDict:dict];
    [self safeSetValue:self.postToFacebook forKey:@"post_to_facebook" inDict:dict];

    return dict;
}

- (void)safeSetValue:(id)value forKey:(NSString *)key inDict:(NSMutableDictionary *)dict
{
    if (value != nil) {
        dict[key] = value;
    }
}

@end
