//
//  NBRunKeeperClient.h
//  Jupiter
//
//  Created by Nicholas Bonatsakis on 2/17/14.
//
//

#import <AFNetworking/AFNetworking.h>

@class NBRunKeeperFitnessActivity;

typedef void(^NBRunKeeperRequestAuthBlock)(BOOL didAuthorize, NSError *error);


@interface NBRunKeeperClient : AFHTTPSessionManager

@property (nonatomic, strong) NSDateFormatter *dateFormatter;

+ (NBRunKeeperClient *)shared;
- (void)initializeWithClientID:(NSString *)clientID clientSecret:(NSString *)clientSecret;
- (void)requestAuthorizationWithCompletion:(NBRunKeeperRequestAuthBlock)completion;
- (BOOL)isAuthorized;
- (void)deauthorizeLocally;

#pragma mark - Data

- (NSURLSessionTask *)postNewFitnessActivity:(NBRunKeeperFitnessActivity *)activity
                       success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                       failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

@end
