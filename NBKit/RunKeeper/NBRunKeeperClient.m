//
//  NBRunKeeperClient.m
//  Jupiter
//
//  Created by Nicholas Bonatsakis on 2/17/14.
//
//

#import "NBRunKeeperClient.h"
#import "UICKeyChainStore.h"
#import "NBRunKeeperAuthorizationViewController.h"
#import "NBRunKeeperFitnessActivity.h"

#define kNBRunKeeperBaseURL           @"https://api.runkeeper.com"
#define kNBRunKeeperTokenEndpointURL  @"https://runkeeper.com/apps/token"
#define kNBRunKeeperUserPath          @"/user"

NSString *const kNBRunKeeperAuthTokenKey = @"NBRunKeeperAuthTokenKey";

@interface NBRunKeeperClient ()

@property (nonatomic, copy) NSString *clientID;
@property (nonatomic, copy) NSString *clientSecret;

@end

@implementation NBRunKeeperClient

+ (NBRunKeeperClient *)shared
{
    static NBRunKeeperClient *s = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        s = [[NBRunKeeperClient alloc] initWithBaseURL:[NSURL URLWithString:kNBRunKeeperBaseURL]];
    });

    return s;
}

- (void)initializeWithClientID:(NSString *)clientID clientSecret:(NSString *)clientSecret
{
    self.dateFormatter = [[NSDateFormatter alloc] init];
    self.dateFormatter.dateFormat = @"EEE, d MMM yyyy hh:mm:ss";
    
    self.clientID = clientID;
    self.clientSecret = clientSecret;
    
    AFJSONRequestSerializer *reqSerializer = [[AFJSONRequestSerializer alloc] init];
    [self setRequestSerializer:reqSerializer];
    
    NSArray *acceptableTypes = @[
                                 @"application/json",
                                 @"application/vnd.com.runkeeper.user+json",
                                 ];
    AFJSONResponseSerializer* respSer = [[AFJSONResponseSerializer alloc] init];
    respSer.acceptableContentTypes = [[NSSet alloc] initWithArray:acceptableTypes];
    [self setResponseSerializer:respSer];
    
    [self configureAuthToken];
}

- (void)configureAuthToken
{
    NSString *authToken = [UICKeyChainStore stringForKey:kNBRunKeeperAuthTokenKey];
    if (authToken) {
        AFJSONRequestSerializer *reqSerialier = (AFJSONRequestSerializer *) [self requestSerializer];
        [reqSerialier setValue:[NSString stringWithFormat:@"Bearer %@", authToken] forHTTPHeaderField:@"Authorization"];
    }
}

#pragma mark - Auth

- (void)requestAuthorizationWithCompletion:(NBRunKeeperRequestAuthBlock)completion
{
    NBRunKeeperAuthorizationViewController *authVC = [[NBRunKeeperAuthorizationViewController alloc] init];
    authVC.clientID = self.clientID;
    
    __weak NBRunKeeperClient *weakSelf = self;
    __weak NBRunKeeperAuthorizationViewController *weakAuthVC = authVC;
    authVC.completion = ^(NSString *code, NSString *redirectURI) {
        
        if (code) {
            [weakSelf requestAuthTokenWithCode:code redirectURI:redirectURI success:^(NSURLSessionDataTask *task, id responseObject) {
                
                NSString *token = responseObject[@"access_token"];
                [UICKeyChainStore setString:token forKey:kNBRunKeeperAuthTokenKey];
                [weakSelf configureAuthToken];

                [weakAuthVC dismissViewControllerAnimated:YES completion:nil];
                if (completion) {
                    completion(YES, nil);
                }
            } failure:^(NSURLSessionDataTask *task, NSError *error) {
                [weakAuthVC dismissViewControllerAnimated:YES completion:nil];
                if (completion) {
                    completion(NO, error);
                }
            }];
        } else if (completion) {
            [weakAuthVC dismissViewControllerAnimated:YES completion:nil];
            completion(NO, nil);
        }
    };
    
    [[weakSelf topMostController] presentViewController:[[UINavigationController alloc] initWithRootViewController:authVC]
                                                        animated:YES
                                                      completion:nil];
}

- (BOOL)isAuthorized
{
    return [UICKeyChainStore stringForKey:kNBRunKeeperAuthTokenKey] != nil;
}

- (void)deauthorizeLocally;
{
    [UICKeyChainStore removeItemForKey:kNBRunKeeperAuthTokenKey];
}

- (UIViewController *)topMostController
{
    UIViewController *topController = [UIApplication sharedApplication].keyWindow.rootViewController;
    
    while (topController.presentedViewController) {
        topController = topController.presentedViewController;
    }
    
    return topController;
}

- (NSURLSessionTask *)requestAuthTokenWithCode:(NSString *)code
                                   redirectURI:(NSString *)redirectURI
                                       success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                                       failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure
{
    NSDictionary *params = @{
                             @"grant_type" : @"authorization_code",
                             @"code" : code,
                             @"client_id" : self.clientID,
                             @"client_secret" : self.clientSecret,
                             @"redirect_uri" : redirectURI
                             };
    
    AFHTTPRequestSerializer *reqSer = [[AFHTTPRequestSerializer alloc] init];
    NSURLRequest *req = [reqSer requestWithMethod:@"POST" URLString:kNBRunKeeperTokenEndpointURL parameters:params error:nil];

    __block NSURLSessionDataTask *task = [self dataTaskWithRequest:req completionHandler:^(NSURLResponse * __unused response, id responseObject, NSError *error) {
        if (error) {
            if (failure) {
                failure(task, error);
            }
        } else {
            if (success) {
                success(task, responseObject);
            }
        }
    }];
    
    [task resume];

    return task;
}

#pragma mark - Activities

- (NSURLSessionTask *)postNewFitnessActivity:(NBRunKeeperFitnessActivity *)activity
                       success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                       failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure
{
    __weak NBRunKeeperClient *weakSelf = self;
    return [self GET:kNBRunKeeperUserPath parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        NSString *fitnessPath = responseObject[@"fitness_activities"];
        
        NSMutableURLRequest *request = [weakSelf.requestSerializer requestWithMethod:@"POST" URLString:[[NSURL URLWithString:fitnessPath relativeToURL:weakSelf.baseURL] absoluteString] parameters:[activity toDict] error:nil];
        [request setValue:@"application/vnd.com.runkeeper.NewFitnessActivity+json" forHTTPHeaderField:@"Content-Type"];
        [weakSelf taskForRequest:request success:success failure:failure];
        
    } failure:failure];
}

- (NSURLSessionTask *)taskForRequest:(NSURLRequest *)request
                             success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                             failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure
{
    __block NSURLSessionDataTask *task = [self dataTaskWithRequest:request completionHandler:^(NSURLResponse * __unused response, id responseObject, NSError *error) {
        if (error) {
            if (failure) {
                failure(task, error);
            }
        } else {
            if (success) {
                success(task, responseObject);
            }
        }
    }];
    
    [task resume];
    
    return task;
}

@end
