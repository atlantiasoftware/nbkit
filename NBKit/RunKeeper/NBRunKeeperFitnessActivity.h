//
//  NBRunKeeperFitnessActivity.h
//  Jupiter
//
//  Created by Nicholas Bonatsakis on 2/17/14.
//
//

#import <Foundation/Foundation.h>

@interface NBRunKeeperFitnessActivity : NSObject

@property (nonatomic, copy) NSString *type;
@property (nonatomic, copy) NSString *secondaryType;
@property (nonatomic, copy) NSString *notes;
@property (nonatomic, copy) NSString *startDate;
@property (nonatomic, strong) NSNumber *durationSec;
@property (nonatomic, strong) NSNumber *totalCalories;
@property (nonatomic, strong) NSNumber *postToTwitter;
@property (nonatomic, strong) NSNumber *postToFacebook;

- (NSDictionary *)toDict;

@end
