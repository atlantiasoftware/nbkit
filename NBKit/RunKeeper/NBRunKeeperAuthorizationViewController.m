//
//  NBRunKeeperAuthorizationViewController.m
//  Jupiter
//
//  Created by Nicholas Bonatsakis on 2/17/14.
//
//

#import "NBRunKeeperAuthorizationViewController.h"

#define kNBRunKeeperAuthURL @"https://runkeeper.com/apps/authorize?response_type=code&client_id=%@&redirect_uri=%@"
#define kRedirectHost @"runkeeperauthredirect"
#define kNBRunKeeperRedirectURL @"http://runkeeperauthredirect"

@interface NBRunKeeperAuthorizationViewController () <UIWebViewDelegate>

@property (nonatomic, weak) IBOutlet UIWebView *webView;
@property (nonatomic, copy) NSString *originalURL;

@end

@implementation NBRunKeeperAuthorizationViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.navigationItem.title = NSLocalizedString(@"Authorize", nil);
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancelTapped:)];

    NSString *urlStr= [NSString stringWithFormat:kNBRunKeeperAuthURL, self.clientID, kNBRunKeeperRedirectURL];
    self.originalURL = urlStr;
    NSURLRequest *req = [NSURLRequest requestWithURL:[NSURL URLWithString:urlStr]];
    [self.webView loadRequest:req];
}

- (void)cancelTapped:(id)sender
{
    if (self.completion) {
        self.completion(nil, nil);
    }
}

#pragma mark - UIWebView

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    if ([[request.URL host] isEqualToString:kRedirectHost]) {
        NSDictionary *params = [self queryParamsFromURL:request.URL];
        
        if (self.completion) {
            self.completion(params[@"code"], kNBRunKeeperRedirectURL);
        }
        
        return NO;
    }
    
    return YES;
}

- (NSDictionary *)queryParamsFromURL:(NSURL *)url
{
    NSMutableDictionary *ret = [NSMutableDictionary dictionary];
    for (NSString *pair in [[url query] componentsSeparatedByString:@"&"]) {
        NSArray *parts = [pair componentsSeparatedByString:@"="];
        ret[parts[0]] = parts[1];
    }
    
    return ret;
}

@end
