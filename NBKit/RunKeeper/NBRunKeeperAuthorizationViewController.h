//
//  NBRunKeeperAuthorizationViewController.h
//  Jupiter
//
//  Created by Nicholas Bonatsakis on 2/17/14.
//
//

#import <UIKit/UIKit.h>

typedef void(^NBRunKeeperAuthorizationBlock)(NSString *authorizationCode, NSString *redirectURI);

@interface NBRunKeeperAuthorizationViewController : UIViewController

@property (nonatomic, copy) NBRunKeeperAuthorizationBlock completion;
@property (nonatomic, copy) NSString *clientID;

@end
