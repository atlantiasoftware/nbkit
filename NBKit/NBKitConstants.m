//
//  NBKitConstants.m
//  NBKit
//
//  Created by Nicholas Bonatsakis on 4/27/15.
//  Copyright (c) 2015 Atlantia Software LLC. All rights reserved.
//

#import "NBKitConstants.h"

// STYLE

@implementation UIFont (NBKGlobalStyle)
+ (UIFont *)nb_alertTitleFontWithSize:(CGFloat)size { return [UIFont fontWithName:@"Avenir-Medium" size:size]; }
+ (UIFont *)nb_alertMessageFontWithSize:(CGFloat)size { return [UIFont fontWithName:@"Avenir-Book" size:size]; }
+ (UIFont *)nb_alertButtonFontWithSize:(CGFloat)size { return [UIFont fontWithName:@"Avenir-Roman" size:size]; }
@end


@implementation NBKitConstants

@end
