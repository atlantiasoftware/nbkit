//
//  NBPrivacyPromptViewController.m
//  NoPhone
//
//  Created by Nicholas Bonatsakis on 1/19/15.
//  Copyright (c) 2015 Atlantia Software LLC. All rights reserved.
//

#import "NBPrivacyPromptViewController.h"
#import "NBPillButton.h"
#import "NBAppearance.h"

@implementation NBPromptStyle
@end


@interface NBPrivacyPromptViewController ()

@property (strong, readwrite, nonatomic) NBPromptStyle *style;

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *messageLabel;
@property (weak, nonatomic) IBOutlet NBPillButton *declineButton;
@property (weak, nonatomic) IBOutlet NBPillButton *acceptButton;
@property (weak, nonatomic) IBOutlet UIImageView *iconImageView;

@end

@implementation NBPrivacyPromptViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.titleLabel.text = self.title;
    self.messageLabel.text = self.message;
    [self.declineButton setTitle:self.declineButtonTitle forState:UIControlStateNormal];
    [self.acceptButton setTitle:self.acceptButtonTitle forState:UIControlStateNormal];
    self.iconImageView.image = self.image;

    [self applyStyle];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

#pragma mark - Style

- (NBPromptStyle *)style
{
    if ( _style == nil ) {
        _style = [[NBPromptStyle alloc] init];
    }
    
    return _style;
}

- (void)applyStyle
{
    self.view.backgroundColor = self.style.backgroundColor;
    self.iconImageView.tintColor = self.style.foregroundColor;
    
    self.titleLabel.font = self.style.titleFont;
    self.titleLabel.textColor = self.style.foregroundColor;
    self.messageLabel.font = self.style.messageFont;
    self.messageLabel.textColor = self.style.foregroundColor;
    
    UIColor *highlightedColor = [self.style.foregroundColor nb_colorWithAlpha:0.6];

    self.acceptButton.titleLabel.font = self.style.acceptButtonFont;
    [self.acceptButton setTitleColor:self.style.foregroundColor forState:UIControlStateNormal];
    [self.acceptButton setTitleColor:highlightedColor forState:UIControlStateHighlighted];
    self.acceptButton.pillFillColor = self.style.tintColor;
    self.acceptButton.highlightedFillColor = [self.style.tintColor nb_colorWithAlpha:0.6];

    self.declineButton.titleLabel.font = self.style.declineButtonFont;
    [self.declineButton setTitleColor:self.style.foregroundColor forState:UIControlStateNormal];
    [self.declineButton setTitleColor:highlightedColor forState:UIControlStateHighlighted];
    self.declineButton.layer.borderWidth = 1.0f;
    self.declineButton.pillFillColor = [UIColor clearColor];
    self.declineButton.pillBorderColor = self.style.foregroundColor;
    self.declineButton.highlightedBorderColor = highlightedColor;
}

#pragma mark - Actions

- (IBAction)declineTapped:(id)sender
{
    if ( self.declineAction ) {
        self.declineAction();
    }
}

- (IBAction)acceptTapped:(id)sender
{
    if ( self.acceptAction ) {
        self.acceptAction();
    }
}

#pragma mark - Privacy Status

+ (BOOL)hasAllowedNotifications
{
    return [[UIApplication sharedApplication] currentUserNotificationSettings].types != 0;
}

+ (BOOL)hasPromptedForNotifications
{
    return [[NSUserDefaults standardUserDefaults] boolForKey:NSStringFromSelector(@selector(hasPromptedForNotifications))];
}

+ (void)markHasPromptedForNotifications
{
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:NSStringFromSelector(@selector(hasPromptedForNotifications))];
}

@end
