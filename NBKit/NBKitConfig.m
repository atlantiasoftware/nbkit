//
//  NBKitConfig.m
//  Jupiter
//
//  Created by Nicholas Bonatsakis on 2/15/14.
//
//

#import "NBKitConfig.h"

@implementation NBKitConfig

+ (NBKitConfig*)shared
{
    static NBKitConfig* cfg = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        cfg = [[NBKitConfig alloc] init];
    
        // ASSERTIONS
        
        NSAssert([[NSBundle mainBundle] infoDictionary][@"CFBundleDisplayName"] != nil, @"NBKIt requires the app display name be set");
    });

    return cfg;
}

- (NSString *)appShortName
{
    if (_appShortName != nil) {
        return _appShortName;
    }
    else {
        return self.appDisplayName;
    }
}

- (NSString*)appURL
{
    NSAssert(self.appID != nil, @"You must set app ID");
    NSString* URLStr = [NSString stringWithFormat:@"http://itunes.apple.com/app/id%@", self.appID];
    if (self.moreAppsAffiliateToken != nil) {
        NSString* ct = [NSString stringWithFormat:@"share_via_%@", self.appDisplayName];
        ct = [ct stringByReplacingOccurrencesOfString:@" " withString:@""];
        URLStr = [URLStr stringByAppendingFormat:@"?at=%@&ct=%@", self.moreAppsAffiliateToken, ct];
    }

    return URLStr;
}

- (NSString*)appDisplayName
{
    if (_appDisplayName != nil) {
        return _appDisplayName;
    }
    else {
        return [[NSBundle mainBundle] infoDictionary][@"CFBundleDisplayName"];
    }
}

- (NSString *)tellFriendMessage
{
    if (_tellFriendMessage == nil) {
        NSString *appName = [self appDisplayName];
        return [NSString stringWithFormat:NSLocalizedString(@"Hey! Check out %@, an awesome iOS app!", nil), appName];
    }
    
    return _tellFriendMessage;
}

- (NSString *)tellFriendPromptMessage
{
    if (_tellFriendPromptMessage == nil) {
        return [NSString stringWithFormat:NSLocalizedString(@"Thanks for checking out %@! If you've enjoyed this app, please let some friends know about it, we really appreciate your support.", nil), self.appDisplayName];
    }
    
    return _tellFriendPromptMessage;
}

- (NSString *)appURLWithAffiliateCampaign:(NSString *)campaign
{
    return [self URLForAppWithID:self.appID campaign:campaign];
}

- (NSString *)URLForAppWithID:(NSString *)appID campaign:(NSString *)campaign
{
    NSAssert(self.appID != nil, @"You must set app ID");
    NSString* URLStr = [NSString stringWithFormat:@"http://itunes.apple.com/app/id%@", appID];
    if (self.moreAppsAffiliateToken != nil) {
        NSString* ct = [NSString stringWithFormat:@"%@_%@", campaign, self.appShortName];
        ct = [ct stringByReplacingOccurrencesOfString:@" " withString:@""];
        URLStr = [URLStr stringByAppendingFormat:@"?at=%@&ct=%@", self.moreAppsAffiliateToken, ct];
    }
    
    return URLStr;
}

@end
