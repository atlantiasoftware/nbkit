//
//  NBKitConstants.h
//  NBKit
//
//  Created by Nicholas Bonatsakis on 4/27/15.
//  Copyright (c) 2015 Atlantia Software LLC. All rights reserved.
//

// STYLE

@interface UIFont (NBKGlobalStyle)
+ (UIFont *)nb_alertTitleFontWithSize:(CGFloat)size;
+ (UIFont *)nb_alertMessageFontWithSize:(CGFloat)size;
+ (UIFont *)nb_alertButtonFontWithSize:(CGFloat)size;
@end

@interface NBKitConstants : NSObject

@end
