//
//  NBAppDelegate.h
//  NBKit
//
//  Created by Nicholas Bonatsakis on 4/14/14.
//  Copyright (c) 2014 Atlantia Software LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NBAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
