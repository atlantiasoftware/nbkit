//
//  NBViewController.m
//  NBKit
//
//  Created by Nicholas Bonatsakis on 4/14/14.
//  Copyright (c) 2014 Atlantia Software LLC. All rights reserved.
//

#import "NBViewController.h"
#import "NBRatingService.h"
#import "NBSharingService.h"

@interface NBViewController ()

@end

@implementation NBViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [[NBSharingService shared] showTellFriend];
    });
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
