//
//  main.m
//  NBKit
//
//  Created by Nicholas Bonatsakis on 4/14/14.
//  Copyright (c) 2014 Atlantia Software LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "NBAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([NBAppDelegate class]));
    }
}
